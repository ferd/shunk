-module(bench).
-compile(export_all).
-define(VSN, 1).

%% @doc see the time it takes to rewrite 10% of the DB, re-chunk and
%% diff.
serv_diff(Count, Pct) ->
    {ok,A} = shunk_tab_serv:start_link(a),
    {ok,B} = shunk_tab_serv:start_link(b),
    [begin
         K = crypto:hash(md5, integer_to_binary(X)),
         V = crypto:hash(md5, integer_to_binary(X+X)),
         shunk_tab_serv:insert(a, K, V),
         shunk_tab_serv:insert(b, K, V)
     end || X <- lists:seq(1,Count)],
     RewriteSet = [
        {crypto:hash(md5, integer_to_binary(rand:uniform(Count))),
         crypto:hash(md5, integer_to_binary(-1))}
        || _ <- lists:seq(1, trunc(Count*Pct))],
     T1 = erlang:monotonic_time(milli_seconds),
     {ok, VsnA, LvlA} = shunk_tab_serv:levels(a, infinity),
     T2 = erlang:monotonic_time(milli_seconds),
     {ok, VsnB1, _LvlB1} = shunk_tab_serv:levels(b, infinity),
     shunk_tab_serv:return_vsn(b, VsnB1),
     T3 = erlang:monotonic_time(milli_seconds),
     [shunk_tab_serv:insert(b, K, V) || {K,V} <- RewriteSet],
     T4 = erlang:monotonic_time(milli_seconds),
     {ok, VsnB2, LvlB2} = shunk_tab_serv:levels(b, infinity),
     T5 = erlang:monotonic_time(milli_seconds),
     Ranges = shunk_snap:diff_lvls(LvlA, LvlB2),
     T6 = erlang:monotonic_time(milli_seconds),
     Diff = lists:append([
      begin
        SubA = shunk_tab_serv:sublevels(a, VsnA, Range),
        SubB = shunk_tab_serv:sublevels(b, VsnB2, Range),
        [Min || {Min,Min} <- shunk_snap:diff_lvls(SubA,SubB)]
      end || Range <- Ranges]),
     T7 = erlang:monotonic_time(milli_seconds),
     unlink(A), unlink(B),
     gen_server:stop(A), gen_server:stop(B),
     io:format("Calculating first levels (a): ~pms~n"
               "Calculating first levels (b): ~pms~n"
               "Inserting ~p% of entries as rewrite: ~pms~n"
               "Rehashing b: ~pms~n"
               "Diffing top chunks: ~pms~n"
               "Diffing subranges (~p): ~pms~n"
               "Diff size: ~p (expected: ~p)~n",
               [T2-T1, T3-T2, 100*Pct, T4-T3, T5-T4, T6-T5,
                length(Ranges), T7-T6,
                length(Diff), length(lists:usort([K || {K,_} <- RewriteSet]))]).

%% @doc mixed full mvcc GC vs. empty GC
%%
%% Experimentation shows mixed GC (~40% rewrites or deletes) is roughly
%% 275% slower to run than a null GC.
nullgc_vs_mixedgc(Count) ->
    T = fill_table(Count),
    T1 = erlang:monotonic_time(milli_seconds),
    shunk_tab:gc(T,?VSN),
    T2 = erlang:monotonic_time(milli_seconds),
    do_ops(T, Count, Count div 3, ?VSN+1),
    do_ops(T, Count, Count div 3, ?VSN+2),
    do_ops(T, Count, Count div 3, ?VSN+3),
    T3 = erlang:monotonic_time(milli_seconds),
    shunk_tab:gc(T,?VSN+1),
    T4 = erlang:monotonic_time(milli_seconds),
    kill_table(T),
    {T2-T1, T4-T3, (T4-T3)/(T2-T1)}.

%% @doc compare time it takes to run a full-scan null GC over
%% a table vs. regular lookup iteration.
%%
%% Experimentation shows the GC run to be 18% slower than iteration
%% with lookups.
lookup_vs_nullgc(Count, N) ->
    T = fill_table(Count),
    Lookup = eministat:s(lookup, fun() -> iterate_lookup(T) end, N),
    GC = eministat:s(gc, fun() -> shunk_tab:gc(T, ?VSN) end, N),
    eministat:x(99.5, Lookup, GC),
    kill_table(T).

%% @doc compare the time it takes to chunk using a regular ETS table vs.
%% one that supports MVCC.
%%
%% Experimentation shows that the overhead is roughly 63%. This means
%% that MVCC iteration is currently what causes a majority of the
%% performance cost when comparing with an empty loop, rather than chunking.
%%
%% The gain in concurrency may or may not be worth it.
chunk_vs_mvcc(Count, N) ->
    T = fill_table(Count),
    Iter = eministat:s(chunk, fun() -> iterate(T) end, N),
    Chunk = eministat:s(mvcc, fun() -> shunk_tab:chunk(T, ?VSN) end, N),
    eministat:x(99.5, Iter, Chunk),
    kill_table(T).

%% @doc compare the time it takes to iterate over a table doing nothing
%% and how long it takes to do it while sharding.
%%
%% Experimental results show this to be 65% slower than iterating MVCC only.
iterate_vs_chunk(Count, N) ->
    T = fill_table(Count),
    Iter = eministat:s(iterate, fun() -> iterate(T) end, N),
    Chunk = eministat:s(chunk, fun() -> shunk_tab:chunk(T, ?VSN) end, N),
    eministat:x(99.5, Iter, Chunk),
    kill_table(T).

%% @doc compare the time it takes to iterate over a table doing nothing
%% but lookups and how long it takes to do it while sharding.
%%
%% Experimental results show sharding to be 246% slower than just
%% iterating and looking up.
lookup_vs_chunk(Count, N) ->
    T = fill_table(Count),
    Lookup = eministat:s(lookup, fun() -> iterate_lookup(T) end, N),
    Chunk = eministat:s(chunk, fun() -> shunk_tab:chunk(T, ?VSN) end, N),
    eministat:x(99.5, Lookup, Chunk),
    kill_table(T).

%% @doc compare the time it takes to iterate over an ETS table as a raw
%% thing and as an MVCC table as supported by shunk.
%%
%% Experimental results show the MVCC step being 70% slower than the opposite,
%% simply because of the iterate-then-read-if-deleted steps.
iterate_vs_empty(Count, N) ->
    T = fill_table(Count),
    Iter = eministat:s(iterate, fun() -> iterate(T) end, N),
    Empty = eministat:s(empty, fun() -> iterate_empty(T) end, N),
    eministat:x(99.5, Iter, Empty),
    kill_table(T).

%% @doc compare the time it takes to iterate over an ETS table as a raw
%% thing + lookups and as an MVCC table as supported by shunk.
%%
%% Experimental results show the MVCC step being 50% slower than its opposite.
%% Should possibly try to find if that can be made smaller.
iterate_vs_lookup(Count, N) ->
    T = fill_table(Count),
    Iter = eministat:s(iterate, fun() -> iterate(T) end, N),
    Lookup = eministat:s(lookup, fun() -> iterate_lookup(T) end, N),
    eministat:x(99.5, Iter, Lookup),
    kill_table(T).

%%%%%%%%%%%%%%%
%%% HELPERS %%%
%%%%%%%%%%%%%%%
iterate(T) -> iterate(T, shunk_tab:first(T, ?VSN)).
iterate(_, '$end_of_table') -> ok;
iterate(T, {K,_}) -> iterate(T, shunk_tab:next(T,?VSN,K)).

iterate_empty(T) -> iterate_empty(T, ets:first(T)).
iterate_empty(_, '$end_of_table') -> ok;
iterate_empty(T, K) -> iterate_empty(T, ets:next(T,K)).

iterate_lookup(T) -> iterate_lookup(T, ets:first(T)).
iterate_lookup(_, '$end_of_table') -> ok;
iterate_lookup(T, K) ->
    [_] = ets:lookup(T, K),
    iterate_lookup(T, ets:next(T,K)).

iterate_chunk(T) ->
    lists:reverse(iterate_chunk(T, ets:first(T), shunk_chunk:init(), 1, [])).
iterate_chunk(T, '$end_of_table', _S, N, Acc) ->
    case ets:last(T) of
        '$end_of_table' -> [];
        K ->
            case Acc of
                [{K,_}|_] -> Acc;
                _ -> [{K,N-1}|Acc]
            end
    end;
iterate_chunk(T, K, S, N, Acc) ->
    [{K,V}] = ets:lookup(T,K),
    case shunk_chunk:chunk([K,V], S) of
        {next, S2} ->
            iterate_chunk(T, ets:next(T,K), S2, N+1, Acc);
        {chunk, Offset, [K2,_], S2} ->
            iterate_chunk(T, ets:next(T,K2), S2, 1, [{K,N+Offset}|Acc])
    end.

fill_table(Count) ->
    T = shunk_tab:new(bench_tab),
    Entries = [{crypto:hash(md5, integer_to_binary(X)), crypto:hash(md5, integer_to_binary(X+X))}
               || X <- lists:seq(1,Count)],
    [shunk_tab:insert(T, K, V, ?VSN) || {K,V} <- Entries],
    T.


kill_table(T) -> ets:delete(T).

do_ops(_, _, 0, _) ->
    ok;
do_ops(T, Count, N, Vsn) ->
    case rand:uniform(5) of
        1 -> % write
            K = crypto:hash(md5, integer_to_binary(rand:uniform(Count))),
            V = crypto:hash(md5, <<Vsn>>),
            shunk_tab:insert(T, K, V, Vsn);
        2 -> % delete
            K = crypto:hash(md5, integer_to_binary(rand:uniform(Count))),
            shunk_tab:delete(T, K, Vsn);
        _ -> % noop
            ok
    end,
    do_ops(T, Count, N-1, Vsn).
