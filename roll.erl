-module(roll).
-compile(export_all).
-define(r, 0). % hash to compare value to
-record(tttd, {window_size = 15 :: pos_integer(),
               buffer = {} :: tuple(),
               nth = 1 :: pos_integer(),
               threshold_low=460 :: pos_integer(),
               threshold_high=2800 :: pos_integer(),
               divisor=540 :: non_neg_integer(),
               backup_divisor=270 :: non_neg_integer(),
               backup_offset=undefined :: undefined | term()}).

interval_to_merklet(T, From, Until) ->
    fold_interval(T, From, Until,
                  fun(KV, Tree) -> merklet:insert(KV, Tree) end,
                  undefined).

md5_interval(T, From, Until) ->
    crypto:hash_final(
        fold_interval(T, From, Until,
                      fun({K,V}, Hash) -> crypto:hash_update(Hash,[K,V]) end,
                      crypto:hash_init(md5))
    ).

adler32_interval(T, From, Until) ->
    fold_interval(T, From, Until,
                  fun({K,V}, Hash) -> erlang:adler32(Hash, [K,V]) end, 1).

adler32_each(T, From, Until) ->
    fold_interval(T, From, Until,
                  fun({K,V}, [H|_]=L) -> [erlang:adler32(H, [K,V])|L] end, [1]).

fold_interval(_T, At, Until, _Fun, State) when At >= Until ->
    State;
fold_interval(T, At, Until, Fun, State) ->
    [Entry] = ets:lookup(T, At),
    NewState = Fun(Entry, State),
    fold_interval(T, ets:next(T, At), Until, Fun, NewState).

shard_window(_, '$end_of_table', _, _, _, _, _,
             _, _, _, Chunks) -> Chunks;
shard_window(T, Key, WindowSize, Buffer, Nth, LowThreshold, HighThreshold,
             Divisor, BackupDivisor, BackupOffset, Chunks) ->
    [{K,V}] = ets:lookup(T, Key),
    Buffer2 = case tuple_size(Buffer) of
        WindowSize ->
            erlang:append_element(erlang:delete_element(1, Buffer), [K,V]);
        _ ->
            erlang:append_element(Buffer, [K,V])
    end,
    if Nth < LowThreshold ->
           %% keep going
           shard_window(T, ets:next(T, Key), WindowSize, Buffer2,
                        Nth+1, LowThreshold, HighThreshold, Divisor,
                        BackupDivisor, BackupOffset, Chunks);
       Nth < HighThreshold ->
           %% grab hashes
           Hash = erlang:adler32(tuple_to_list(Buffer2)),
           case Hash rem Divisor =:= ?r of
               true ->
                   %% Chunk end, reset buffer, backup offset, counter
                   shard_window(T, ets:next(T, Key), WindowSize, {},
                                1, LowThreshold, HighThreshold, Divisor,
                                 BackupDivisor, undefined, [{Key,Nth}|Chunks]);
               false ->
                   NewBackupOffset = case BackupOffset of
                       undefined when Hash rem BackupDivisor =:= ?r ->
                           {Key,Nth,Hash,Buffer2};
                       _ ->
                           BackupOffset
                   end,
                   %% Keep going
                   shard_window(T, ets:next(T, Key), WindowSize, Buffer2,
                                Nth+1, LowThreshold, HighThreshold, Divisor,
                                BackupDivisor, NewBackupOffset, Chunks)
           end;
       Nth >= HighThreshold, BackupOffset =:= undefined ->
           %% Current value is a forced limit
           shard_window(T, ets:next(T, Key), WindowSize, {},
                        1, LowThreshold, HighThreshold, Divisor,
                        BackupDivisor, undefined, [{Key,Nth}|Chunks]);
       Nth >= HighThreshold ->
           %% Roll back to the old backup offset
           {OldKey,OldNth,_OldHash,_OldBuffer} = BackupOffset,
           shard_window(T, ets:next(T, OldKey), WindowSize, {},
                        1, LowThreshold, HighThreshold, Divisor,
                        BackupDivisor, undefined, [{OldKey,OldNth}|Chunks])
    end.

shard_alt_window(T) ->
    Key = ets:first(T),
    State = init_shard(),
    shard_alt_window(T, Key, State, []).

shard_alt_window(T, '$end_of_table', State, Chunks) ->
    Last = ets:last(T),
    case hd(Chunks) of
        Last -> Chunks; % covered all entries
        _ -> [Last|Chunks] % pick the last value as a chunk boundary
    end;
shard_alt_window(T, Key, State, Chunks) ->
    [{K,V}] = ets:lookup(T, Key),
    case shard([K,V], State) of
        {chunk, _N, [KChunk,_], NewState} ->
            shard_alt_window(T, ets:next(T, KChunk), NewState, [KChunk|Chunks]);
        {next, NewState} ->
            shard_alt_window(T, ets:next(T, K), NewState, Chunks)
    end.

init_shard() -> #tttd{}.

-spec(Data, #tttd{}) -> {chunk, Offset, Data, #tttd{}} | {next, #tttd{}} when
      Data :: iodata(),
      %% when picking a chunk, either the current value is picked, or if we hit
      %% a sliding window limit, the backup divisor picks a backup offset.
      %% an offset of 0 means the current value was picked. A negative one of
      %% `-N' means that the data was `N' calls prior.
      Offset :: 0 | neg_integer().

shard(IoData, T=#tttd{window_size=WindowSize, buffer=Buffer, nth=Nth,
                      threshold_low=Low, threshold_high=High,
                      divisor=D1, backup_divisor=D2, backup_offset=Backup}) ->
    Buffer2 = case tuple_size(Buffer) of
        WindowSize ->
            erlang:append_element(erlang:delete_element(1, Buffer), IoData);
        _ ->
            erlang:append_element(Buffer, IoData)
    end,
    if Nth < Low ->
           %% keep going
           {next, T#tttd{buffer=Buffer2, nth=Nth+1}};
       Nth < High ->
           %% grab hashes
           Hash = erlang:adler32(tuple_to_list(Buffer2)),
           case Hash rem D1 =:= ?r of
               true ->
                   %% Chunk end, reset buffer, backup offset, counter
                   {chunk, 0, IoData,
                    T#tttd{buffer={}, nth=1, backup_offset=undefined}};
               false ->
                   NewBackup = case Backup of
                       undefined when Hash rem D2 =:= ?r ->
                           {IoData,Nth,Hash,Buffer2};
                       _ ->
                           Backup
                   end,
                   %% Keep going
                   {next, T#tttd{buffer=Buffer2, nth=Nth+1,
                                 backup_offset=NewBackup}}
           end;
       Nth >= High, Backup =:= undefined ->
           %% Current value is a forced limit
           {chunk, 0, IoData,
            T#tttd{buffer={}, nth=1, backup_offset=undefined}};
       Nth >= High ->
           %% Roll back to the old backup offset
           {OldIo,OldNth,_OldHash,_OldBuffer} = Backup,
           {chunk, OldNth-Nth, OldIo,
            T#tttd{buffer={}, nth=1, backup_offset=undefined}}
    end.


%% @doc adler32 fixed window vs empty
empty_vs_adler_window(Count, N) ->
    T = fill_table(Count),
    Next = eministat:s(next, fun() -> next_empty(T) end, N),
    Adler = eministat:s(window_adler, fun() -> window_adler(T, 50) end, N),
    eministat:x(99.5, Next, Adler),
    kill_table(T).

%% @doc adler32 fixed window (50n) vs per-element
fixed_vs_single(Count, N) ->
    T = fill_table(Count),
    Single = eministat:s(single, fun() -> next_adler(T) end, N),
    Window = eministat:s(window, fun() -> window_adler(T, 50) end, N),
    eministat:x(99.5, Single, Window),
    kill_table(T).

window_adler(T, WindowSize) ->
    L = lists:seq(0, WindowSize-1),
    [put({window,N}, <<>>) || N <- L],
    K = ets:first(T),
%    R = window_adler(T, K, WindowSize, 0),
    R = window_adler(T, K, WindowSize, {}),
    [erase({window,N}) || N <- L],
    R.

window_adler(_T, '$end_of_table', _WindowSize, _) -> ok;
window_adler(T, Key, WindowSize, N) ->
    [{K,V}] = ets:lookup(T, Key),
%    Current = N rem WindowSize,
    N2 = case tuple_size(N) of
        WindowSize ->
            erlang:append_element(erlang:delete_element(1, N), [K,V]);
        _ ->
            erlang:append_element(N, [K,V])
    end,
    erlang:adler32(tuple_to_list(N2)),
    window_adler(T, ets:next(T, Key), WindowSize, N2).
%    put({window, Current}, [K,V]),
%    _A = erlang:adler32(window(Current, WindowSize)),
%    window_adler(T, ets:next(T, Key), WindowSize, Current+1).

%window(Current, WindowSize) ->
%    window(Current rem WindowSize, (Current+1) rem WindowSize, WindowSize).
%
%window(End, End, _Size) ->
%    Entry = get({window, End}),
%    [Entry];
%window(End, Current, Size) ->
%    Entry = get({window, Current}),
%    [Entry | window(End, (Current+1) rem Size, Size)].

%% @doc calcualting a rolling hash with adler32 vs. md5
adler_vs_md5(Count, N) ->
    T = fill_table(Count),
    Adler = eministat:s(adler, fun() -> next_adler(T) end, N),
    MD5 = eministat:s(md5, fun() -> next_md5(T) end, N),
    eministat:x(99.5, Adler, MD5),
    kill_table(T).

next_md5(T) ->
    K = ets:first(T),
    next_md5(T, K, crypto:hash_init(md5)).

next_md5(_, '$end_of_table', A) -> A;
next_md5(T, Key, An_1) ->
    [{K,V}] = ets:lookup(T, Key),
    A = crypto:hash_update(An_1, [K,V]),
    crypto:hash_final(A), % additional cost during the sharding
    next_md5(T, ets:next(T, Key), A).


%% @doc calcualting a rolling hash with adler32 vs. empty loop
empty_vs_adler(Count, N) ->
    T = fill_table(Count),
    Next = eministat:s(next, fun() -> next_empty(T) end, N),
    Adler = eministat:s(adler, fun() -> next_adler(T) end, N),
    eministat:x(99.5, Next, Adler),
    kill_table(T).

next_adler(T) ->
    K = ets:first(T),
    next_adler(T, K, 1).

next_adler(_, '$end_of_table', A) -> A;
next_adler(T, Key, An_1) ->
    [{K,V}] = ets:lookup(T, Key),
    A = erlang:adler32(An_1, [K,V]),
    next_adler(T, ets:next(T, Key), A).

%% @doc iterating with `ets:next/2' is 8% faster than folds
fold_vs_next(Count, N) ->
    T = fill_table(Count),
    Fold = eministat:s(fold, fun() -> fold_empty(T) end, N),
    Next = eministat:s(next, fun() -> next_empty(T) end, N),
    eministat:x(99.5, Fold, Next),
    kill_table(T).

fold_empty(T) ->
    ets:foldl(fun(_, Acc) -> Acc end, ok, T).

next_empty(T) ->
    K = ets:first(T),
    next_empty(T, K).

next_empty(_, '$end_of_table') -> ok;
next_empty(T, Key) ->
    [_] = ets:lookup(T, Key),
    next_empty(T, ets:next(T, Key)).

fill_table(Count) ->
    T = ets:new(bench_tab, [ordered_set, protected]),
    Entries = [{crypto:hash(md5, integer_to_binary(X)), crypto:hash(md5, integer_to_binary(X+X))}
               || X <- lists:seq(1,Count)],
    ets:insert(T, Entries),
    T.

kill_table(T) -> ets:delete(T).

