-module(prop_shunk_tab).
-include_lib("proper/include/proper.hrl").

-define(vsn_range, {1,10}). % no use going bigger
-define(window_size, 15).
-define(threshold_low, 460).
-define(threshold_high, 2800).
-define(d1, 540).
-define(d2, 270).

-export([fill_vsn_tab/2, fill_flat_tab/3,
         gc_entries/2, entries_at/2]). % useful in the shell to debug

%%%%%%%%%%%%%%%%%%%%
%%% TABLE MOTION %%%
%%%%%%%%%%%%%%%%%%%%
%% With support of MVCC operations.

prop_first() ->
    ?FORALL({MaxVsn, Entries}, {version(?vsn_range), entries(?vsn_range)},
            begin
                Tv = shunk_tab:new(versioned),
                Tf = shunk_tab:new(flattened),
                fill_vsn_tab(Tv, Entries),
                fill_flat_tab(Tf, MaxVsn, Entries),
                FirstVsn = shunk_tab:first(versioned, MaxVsn),
                FlatFirst = case ets:first(flattened) of
                    '$end_of_table' ->
                        '$end_of_table';
                    K ->
                        [{K,_,Vsn}] = ets:lookup(flattened, K),
                        {K,Vsn}
                end,
                ets:delete(Tv),
                ets:delete(Tf),
                ?WHENFAIL(
                   io:format("Versioned: ~p~nFlat: ~p~n", [FirstVsn, FlatFirst]),
                   FirstVsn =:= FlatFirst
                )
            end).

prop_last() ->
    ?FORALL({MaxVsn, Entries}, {version(?vsn_range), entries(?vsn_range)},
            begin
                Tv = shunk_tab:new(versioned),
                Tf = shunk_tab:new(flattened),
                fill_vsn_tab(Tv, Entries),
                fill_flat_tab(Tf, MaxVsn, Entries),
                LastVsn = shunk_tab:last(versioned, MaxVsn),
                FlatLast = case ets:last(flattened) of
                    '$end_of_table' ->
                        '$end_of_table';
                    K ->
                        [{K,_,Vsn}] = ets:lookup(flattened, K),
                        {K,Vsn}
                end,
                ets:delete(Tv),
                ets:delete(Tf),
                ?WHENFAIL(
                   io:format("Versioned: ~p~nFlat: ~p~n", [LastVsn, FlatLast]),
                   LastVsn =:= FlatLast
                )
            end).

prop_next() ->
    ?FORALL({MaxVsn, Entries}, {version(?vsn_range), entries(?vsn_range)},
            begin
                Tv = shunk_tab:new(versioned),
                Tf = shunk_tab:new(flattened),
                fill_vsn_tab(Tv, Entries),
                fill_flat_tab(Tf, MaxVsn, Entries),
                Key = pick_random(Entries),
                NextVsn = (catch shunk_tab:next(versioned, MaxVsn, Key)),
                FlatNext = case ets:next(flattened, Key) of
                    '$end_of_table' ->
                        '$end_of_table';
                    K ->
                        [{K,_,Vsn}] = ets:lookup(flattened, K),
                        {K,Vsn}
                end,
                ets:delete(Tv),
                ets:delete(Tf),
                ?WHENFAIL(
                   io:format("Key: ~p~nVersioned: ~p~nFlat: ~p~n", [Key, NextVsn, FlatNext]),
                   NextVsn =:= FlatNext
                )
            end).

prop_lookup() ->
    ?FORALL({MaxVsn, Entries}, {version(?vsn_range), entries(?vsn_range)},
            begin
                Tv = shunk_tab:new(versioned),
                Tf = shunk_tab:new(flattened),
                fill_vsn_tab(Tv, Entries),
                fill_flat_tab(Tf, MaxVsn, Entries),
                Key = pick_random(Entries),
                VsnVal = (catch shunk_tab:lookup(versioned, MaxVsn, Key)),
                FlatVal = case ets:lookup(flattened, Key) of
                    [{_,Val,_}] -> {ok,Val};
                    _ -> {error, not_found}
                end,
                ets:delete(Tv),
                ets:delete(Tf),
                ?WHENFAIL(
                   io:format("Key: ~p~nVersioned: ~p~nFlat: ~p~n", [Key, VsnVal, FlatVal]),
                   VsnVal =:= FlatVal
                )
            end).

%%%%%%%%%%%%%%%%%%%%%%
%%% TABLE CHUNKING %%%
%%%%%%%%%%%%%%%%%%%%%%

%% @doc All thresholds for a given section of chunks is within the bounds
%% of `[Low, High]', with the exception of the last chunk, which may be
%% shorter since the data set size may not be sufficient to fill the
%% minimum threshold.
prop_thresholds() ->
    ?FORALL({{Low, High}, Vsn, Entries},
            {thresholds(), version(?vsn_range), entries(?vsn_range)},
            begin
                T = shunk_tab:new(chunks),
                fill_vsn_tab(T, Entries),
                AllChunks = shunk_tab:chunk(T, Vsn,
                                            ?window_size, Low, High, ?d1, ?d2),
                ets:delete(T),
                case lists:reverse(AllChunks) of
                    [] ->
                        true; % empty, we're okay
                    [Last={_,_,LastN}|Chunks] ->
                        ?WHENFAIL(io:format("Low: ~p~nHigh: ~p~nChunks:~n~p~n",
                                            [Low, High, lists:reverse([Last|Chunks])]),
                            lists:all(fun({_Start,_End,N}) -> N >= Low andalso N =< High end, Chunks)
                            andalso
                            %% Last can be smaller than the low threshold if there's no data left
                            LastN =< High
                        )
                end
            end).

%% @doc All thresholds account for the total of all entries
prop_chunk_sums() ->
    ?FORALL({{Low, High}, Vsn, Entries},
            {thresholds(), version(?vsn_range), entries(?vsn_range)},
            begin
                T = shunk_tab:new(chunks),
                fill_vsn_tab(T, Entries),
                Chunks = shunk_tab:chunk(T, Vsn,
                                         ?window_size, Low, High, ?d1, ?d2),
                ets:delete(T),
                L = entries_at(Vsn, Entries),
                ?WHENFAIL(
                   io:format("~p vs. ~p~n", [Chunks, L]),
                   length(L) =:= lists:sum([N || {_,_,N} <- Chunks])
                )
            end).

%% @doc All ranges where entries have been added and marked as
%% dirty are rewritten. There may be more dirty entries than
%% before, but not more clean entries than there were.
%% Thresholds and boundaries are still accurate.
%% We can validate these properties by ensuring that a partial
%% re-chunk is equivalent to a full chunking.
prop_partial() ->
    ?FORALL({{Low, High}, Vsn, {Entries,Modified}},
            {thresholds(), version(?vsn_range), modified_entries(?vsn_range)},
            begin
                T = shunk_tab:new(chunks),
                %% Get the initial chunk set
                fill_vsn_tab(T, Entries),
                Chunks = shunk_tab:chunk(T, Vsn,
                                         ?window_size, Low, High, ?d1, ?d2),
                %% Get the full re-chunk
                fill_vsn_tab(T, Modified),
                ReChunks = shunk_tab:chunk(T, Vsn,
                                           ?window_size, Low, High, ?d1, ?d2),
                %% Create the dynamic chunk sets marked as dirty
                Dirty = shunk_tab:mark_dirty(Chunks, [element(1,X) || X <- Modified]),
                %% Do a partial rechunk
                PChunks = shunk_tab:partial_chunk(T, Vsn, Dirty,
                                                  ?window_size, Low, High, ?d1, ?d2),
                ets:delete(T),
                %% Compare the partial re-chunk to the full re-chunk
                ?WHENFAIL(
                   io:format("Initial:~p~nModified:~p~nMarked:~p~n~p~nvs.~n~p~n",
                             [Chunks, Modified, Dirty, ReChunks, PChunks]),
                   %% clear out clean/dirty annotations
                   ReChunks =:= [element(2,X) || X <- PChunks]
                )
            end).


%%%%%%%%%%%%%%%%
%%% TABLE GC %%%
%%%%%%%%%%%%%%%%

%% Compare tab GC with a model two-passes GC over the list
prop_gc() ->
    ?FORALL({Vsn, Entries}, {version(?vsn_range), entries(?vsn_range)},
            begin
                T = shunk_tab:new(chunks),
                fill_vsn_tab(T, Entries),
                GCd = gc_entries(Entries, Vsn),
                shunk_tab:gc(T, Vsn),
                GCdTab = tab2format(ets:tab2list(T)),
                ets:delete(T),
                ?WHENFAIL(
                   io:format("Tab: ~p~nEntries: ~p~n", [GCdTab, GCd]),
                   GCdTab =:= GCd
                )
            end).


%%%%%%%%%%%%%%%
%%% FOLDING %%%
%%%%%%%%%%%%%%%
prop_fold() ->
    ?FORALL({MaxVsn, Entries},
            %% This test only works on non-empty sequences.
            ?SUCHTHAT({M,E}, {version(?vsn_range), entries(?vsn_range)},
                      [] =/= entries_at(M,E)),
            begin
                T = shunk_tab:new(chunks),
                fill_vsn_tab(T, Entries),
                L = lists:sort(entries_at(MaxVsn, Entries)),
                %% Pick two random points in the list as arbitrary
                %% start/end spots
                Len = length(L),
                [Start,End] = lists:sort([
                    element(1,lists:nth(rand:uniform(Len), L)),
                    element(1,lists:nth(rand:uniform(Len), L))
                ]),
                %% Fold as an accumulator
                Fold = lists:foldl(
                    fun({K,V}, Acc) when K >= Start, K =< End ->
                            [{K,V}|Acc];
                       (_, Acc) ->
                            Acc
                    end, [], L),
                %% Same for the table
                TabFold = (catch shunk_tab:fold(
                    T, MaxVsn, Start, End,
                    fun(K, V, Acc) -> [{K,V}|Acc] end, []
                )),
                ets:delete(T),
                ?WHENFAIL(
                   io:format("range: ~p~nfold: ~p~ntab: ~p~n",
                             [{Start,End}, Fold, TabFold]),
                   Fold =:= TabFold
                )
            end).

%%%%%%%%%%%%%%%
%%% HELPERS %%%
%%%%%%%%%%%%%%%
fill_vsn_tab(T, Entries) ->
    [case V of
         deleted -> shunk_tab:delete(T, K, Vsn);
         _ -> shunk_tab:insert(T, K, V, Vsn)
     end || {K,Vsn,V} <- Entries],
    ok.

fill_flat_tab(T, MaxVsn, Entries) ->
    [case V of
         deleted -> ets:delete(T, K);
         _ -> ets:insert(T, {K,V,Vsn})
     end || {K,Vsn,V} <- Entries, Vsn =< MaxVsn],
    ok.

pick_random(Entries) ->
    case length(Entries) of
        0 ->
            fake_value;
        N ->
            {K,_,_} = lists:nth(rand:uniform(N), Entries),
            K
    end.

entries_at(MaxVsn, List) ->
    lists:foldl(
      fun({K,Vsn,V}, [{K,_}|Acc]) when Vsn =< MaxVsn ->
              case V of
                  deleted -> Acc;
                  _ -> [{K,V}|Acc]
              end;
         ({K,Vsn,V}, Acc) when Vsn =< MaxVsn, V =/= deleted ->
              [{K,V}|Acc];
         (_, Acc) ->
              Acc
      end,
      [],
      lists:sort(List)).

%%%%%%%%%%%%%%%%%%
%%% GENERATORS %%%
%%%%%%%%%%%%%%%%%%
version({Min,Max}) ->
    integer(Min,Max).

entries(Range) ->
    ?LET(L,
        ?SIZED(Size, resize(Size*10, list(
            {key(), version(Range), value()}
        ))),
        lists:sort(L)
    ).

key() ->
    ?LET(N, integer(1,100), integer_to_binary(N)).

value() ->
    weighted_union([
        {1, deleted},
        {40, non_empty(binary())}
    ]).

modified_entries(Range) ->
    modified_entries(Range, {0, [], []}).

modified_entries(Range, {N, A, B}) ->
    weighted_union([
        %% end
        {1, {A, B}},
        %% original only
        {20, ?LET(Entry, modified_entry(N, Range),
                 ?LAZY(modified_entries(Range, {N+1, [Entry|A], B})))},
        %% overwritten value
        {5, ?LET({Key,Vsn,Val}, modified_entry(N, Range),
                  ?LAZY(modified_entries(
                          Range, {N+1, [{Key,Vsn,Val}|A],
                                       [{Key,Vsn,rewrite(Val)}|B]}
                )))},
        %% added value
        {5, ?LAZY(modified_entries(Range, {N+1, A, [modified_entry(N, Range)|B]}))}
    ]).

rewrite(deleted) -> <<0>>;
rewrite(Bin) when is_binary(Bin) -> <<0, Bin/binary>>.

modified_entry(N, Range) ->
    {integer_to_binary(N), version(Range), value()}.

thresholds() ->
    ?SUCHTHAT({X,Y}, {pos_integer(), pos_integer()}, X < Y).

%% Only drop versions if there's a newer one!
gc_entries(Entries, MaxVsn) ->
    %% filter out outdated entries
    List = lists:foldl(
             fun({K,Vsn,V}, [{K,Vsn,_}|Acc]) ->
                     %% remove generated dupes
                     [{K,Vsn,V}|Acc];
                ({K,Vsn,V}, [{K,OldVsn,_}|Acc]) when OldVsn < MaxVsn ->
                     %% Keep newer only
                     [{K,Vsn,V}|Acc];
                ({K,Vsn,V}, Acc) ->
                     [{K,Vsn,V}|Acc]
             end,
             [], lists:sort(Entries)),
    lists:sort(filter_deleted(lists:sort(List))).

filter_deleted(Entries) ->
    filter_deleted(Entries, {make_ref(), all_deleted}, [], []).

filter_deleted([], {_, all_deleted}, _, Acc) ->
    Acc;
filter_deleted([], {_, other}, Tmp, Acc) ->
    Tmp++Acc;
filter_deleted([Entry={K,_,V}|Entries], {K,all_deleted}, Tmp, Acc) ->
    case V of
        deleted ->
            filter_deleted(Entries, {K,all_deleted}, [Entry|Tmp], Acc);
        _ ->
            %% drop the ones that were deleted before this write
            filter_deleted(Entries, {K,other}, [Entry], Acc)
    end;
filter_deleted([Entry={K,_,_}|Entries], {K,other}, Tmp, Acc) ->
    filter_deleted(Entries, {K,other}, [Entry|Tmp], Acc);
filter_deleted([Entry={K,_,_}|Entries], {_,all_deleted}, _Tmp, Acc) ->
    filter_deleted([Entry|Entries], {K,all_deleted}, [], Acc);
filter_deleted([Entry={K,_,_}|Entries], {_,other}, Tmp, Acc) ->
    filter_deleted([Entry|Entries], {K,all_deleted}, [], Tmp++Acc).

tab2format([]) -> [];
tab2format([{{K,Vsn}, deleted}|T]) -> [{K,Vsn,deleted}|tab2format(T)];
tab2format([{{K,Vsn}, {value,V}}|T]) -> [{K,Vsn,V}|tab2format(T)].
