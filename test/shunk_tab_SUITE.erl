-module(shunk_tab_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").
-compile(export_all).

all() -> [gc, prev_inf_loop].

init_per_testcase(_, Config) ->
    T = shunk_tab:new(gc),
    [{tid, T} | Config].

end_per_testcase(_, Config) ->
    ets:delete(?config(tid, Config)).

%% Implements the test for the comments on the shunk_tab gc/7 function
gc(Config) ->
    T = ?config(tid, Config),
    Full = [
        {<<"X">>, [{1,<<"a">>}, {2,<<"b">>}]},
        {<<"Y">>, [{1,<<"a">>}, {2, deleted}, {4, <<"b">>}, {5, <<"c">>}]},
        {<<"Z">>, [{1,<<"a">>}, {3, deleted}]},
        {<<"W">>, [{1,<<"a">>}, {2, <<"b">>}, {5, <<"c">>}]},
        {<<"R">>, [{1,<<"a">>}, {2, deleted}]},
        {<<"S">>, [{3,<<"a">>}, {4, deleted}, {5, deleted}, {6, deleted}, {7, <<"b">>}]},
        {<<"T">>, [{4,<<"a">>}, {5, deleted}, {6, deleted}, {7, <<"b">>}]},
        {<<"U">>, [{4, deleted}, {5, deleted}, {6, deleted}]},
        {<<"V">>, [{4, deleted}, {5, deleted}, {6, <<"a">>}]}
    ],
    do_ops(T, Full),
    shunk_tab:gc(T, 4),
    ?assertEqual([{2, <<"b">>}], match(T, <<"X">>)),
    ?assertEqual([{4, <<"b">>}, {5, <<"c">>}], match(T, <<"Y">>)),
    ?assertEqual([], match(T, <<"Z">>)),
    ?assertEqual([{5, <<"c">>}], match(T, <<"W">>)),
    ?assertEqual([], match(T, <<"R">>)),
    ?assertEqual([{7, <<"b">>}], match(T, <<"S">>)),
    ?assertEqual([{4, <<"a">>}, {7, <<"b">>}], match(T, <<"T">>)),
    ?assertEqual([], match(T, <<"U">>)),
    ?assertEqual([{6, <<"a">>}], match(T, <<"V">>)).

prev_inf_loop(Config) ->
    T = ?config(tid, Config),
    shunk_tab:insert(T, <<"mykey">>, <<"myval">>, 1),
    shunk_tab:delete(T, <<"otherkey">>, 1),
    Parent = self(),
    spawn_link(fun() -> Parent ! {ok, shunk_tab:chunk(T, 1)} end),
    receive
        {ok, _} -> ok
    after 1000 ->
         error(loop_timeout)
    end,
    ok.



do_ops(T, List) ->
    [do_op(T, Key, Op) || {Key, Ops} <- List, Op <- Ops],
    ok.

do_op(T, Key, {Vsn, deleted}) -> shunk_tab:delete(T, Key, Vsn);
do_op(T, Key, {Vsn, Val}) -> shunk_tab:insert(T, Key, Val, Vsn).

match(T, Key) ->
    Matches = ets:select(T, [{{{Key,'$1'}, {value,'$2'}},
                              [],
                              [['$1','$2']]
                             }]),
    [list_to_tuple(L) || L <- Matches].
