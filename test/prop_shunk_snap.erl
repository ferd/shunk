-module(prop_shunk_snap).
-include_lib("proper/include/proper.hrl").
-define(vsn_range, {1,10}). % no use going bigger

prop_diff() ->
    ?FORALL({MaxVsn, {EntriesA, EntriesB}},
            {version(?vsn_range), entries(?vsn_range)},
            begin
                A = shunk_tab:new(a),
                B = shunk_tab:new(b),
                Pad = pad(10000),
                AllA = Pad ++ EntriesA,
                AllB = Pad ++ EntriesB,
                fill_vsn_tab(A, AllA),
                fill_vsn_tab(B, AllB),
                CA = shunk_tab:chunk(A, MaxVsn),
                CB = shunk_tab:chunk(B, MaxVsn),
                LA = shunk_snap:to_levels(A, MaxVsn, CA),
                LB = shunk_snap:to_levels(B, MaxVsn, CB),
                %% We could piggy back here by doing a partial re-hash
                %% and ensure the partial re-hash yields `LB'.
                %% Requires creating a diffset between `EntriesA' and
                %% `EntriesB' like in `prop_shunk_tab'
                Ranges = shunk_snap:diff_lvls(LA, LB),
                Diff = (entries_at(MaxVsn, AllA) -- entries_at(MaxVsn, AllB))
                    ++ (entries_at(MaxVsn, AllB) -- entries_at(MaxVsn, AllA)),
                case in_range(Diff, Ranges) of
                    true ->
                        ok;
                    false ->
                        io:format("~p~n~p~n", [entries_at(MaxVsn, EntriesA--EntriesB),
                                               entries_at(MaxVsn, EntriesB--EntriesA)]),
                        io:format("~p~n", [{Diff, Ranges}])
                        %error({Diff, Ranges})
                end,
                TDiff = lists:flatten([
                  begin
                    MA = shunk_snap:to_mlevels(A, MaxVsn, Range),
                    MB = shunk_snap:to_mlevels(B, MaxVsn, Range),
                    %% TODO: if the range doesn't exist locally, the
                    %% diff omits them.
                    [Min || {Min,Min} <- shunk_snap:diff_lvls(MA,MB)]
                  end || Range <- Ranges
                ]),
                ets:delete(A),
                ets:delete(B),
                ?WHENFAIL(
                   io:format("Diff: ~p~nTDiff: ~p~n~p~n~p~n",
                             [lists:usort([element(1,X) || X <- Diff]),
                              TDiff, {AllA, AllB},
                              {entries_at(MaxVsn,AllA),entries_at(MaxVsn,AllB)}]),
                   lists:usort([element(1,X) || X <- Diff]) =:= TDiff
                )
            end).

%% @doc show equivalence between a full re-hash and a partial re-hash.
%% Doing so should ensure that if the diff procedure is still correct,
%% then partial re-hash is a valid way to get there still.
prop_partial_hash() ->
    ?FORALL({Vsn, {Entries,Modified}},
            {version(?vsn_range), modified_entries(?vsn_range)},
            begin
                A = shunk_tab:new(a),
                Pad = pad(10000),
                AllA = Pad ++ Entries,
                fill_vsn_tab(A, AllA),
                C1 = shunk_tab:chunk(A, Vsn),
                L1 = shunk_snap:to_levels(A, Vsn, C1),
                fill_vsn_tab(A, Modified),
                CDirty = shunk_tab:mark_dirty(C1, [element(1,X) || X <- Modified]),
                CPartial = shunk_tab:partial_chunk(A, Vsn, CDirty),
                LPartial = shunk_snap:partial_to_levels(A, Vsn, CPartial, lists:last(L1)),
                C2 = shunk_tab:chunk(A, Vsn),
                L2 = shunk_snap:to_levels(A, Vsn, C2),
                ets:delete(A),
                ?WHENFAIL(
                   io:format("dirty: ~p~nlevels: ~p~n",
                             [CDirty,%[X || {dirty,X} <- CDirty],
                              lists:zip(LPartial, L2)]),
                   LPartial =:= L2
                )
            end).


%%% HELPERS
fill_vsn_tab(T, Entries) ->
    [case V of
         deleted -> shunk_tab:delete(T, K, Vsn);
         _ -> shunk_tab:insert(T, K, V, Vsn)
     end || {K,Vsn,V} <- Entries],
    ok.

in_range([], _) -> true;
in_range([{K,_}|T], Ranges) ->
    key_in_range(K, Ranges)
    andalso
    in_range(T, Ranges).

key_in_range(_, []) -> false;
key_in_range(K, [{A,B}|_]) when K >= A, K =< B -> true;
key_in_range(K, [_|T]) -> key_in_range(K, T).

entries_at(MaxVsn, List) ->
    lists:foldl(
      fun({K,Vsn,V}, [{K,_}|Acc]) when Vsn =< MaxVsn ->
              [{K,V}|Acc];
              %case V of
              %    deleted -> Acc;
              %    _ -> [{K,V}|Acc]
              %end;
         ({K,Vsn,V}, [{_, deleted}|Acc]) when Vsn =< MaxVsn -> % flush deleted
              [{K,V}|Acc];
         ({K,Vsn,V}, Acc) when Vsn =< MaxVsn ->
              [{K,V}|Acc];
         (_, Acc) ->
              Acc
      end,
      [],
      %% Need a stable sort to ensure overwrites are not crushed.
      %% There is a need for the version to be considered too, but
      %% that isn't really doable here without a custom sorting
      %% algorithm.
      lists:keysort(1,List)).

%%% GENERATORS
version({Min,Max}) ->
    integer(Min,Max).

entries(Range) ->
    entries(Range, {0, [], []}).

pad(0) -> [];
pad(N) -> [{integer_to_binary(N), 1, <<"pad">>} | pad(N-1)].

entries(Range, {N, A, B}) ->
    weighted_union([
        %% end
        {1, {A, B}},
        %% add to both
        {20, ?LET(Entry, entry(N, Range),
                  ?LAZY(entries(Range, {N+1, [Entry|A], [Entry|B]})))},
        %% add to A
        {5, ?LAZY(entries(Range, {N+1, [entry(N, Range)|A], B}))},
        %% add to B
        {5, ?LAZY(entries(Range, {N+1, A, [entry(N, Range)|B]}))}
    ]).

entry(N, Range) ->
    {integer_to_binary(N), version(Range), value()}.

value() ->
    weighted_union([
        {1, deleted},
        {40, non_empty(binary())}
    ]).

modified_entries(Range) ->
    modified_entries(Range, {0, [], []}).

modified_entries(Range, {N, A, B}) ->
    weighted_union([
        %% end
        {1, {A, B}},
        %% original only
        {20, ?LET(Entry, modified_entry(N, Range),
                 ?LAZY(modified_entries(Range, {N+1, [Entry|A], B})))},
        %% overwritten value
        {5, ?LET({Key,Vsn,Val}, modified_entry(N, Range),
                  ?LAZY(modified_entries(
                          Range, {N+1, [{Key,Vsn,Val}|A],
                                       [{Key,Vsn,rewrite(Val)}|B]}
                )))},
        %% added value
        {5, ?LAZY(modified_entries(Range, {N+1, A, [modified_entry(N, Range)|B]}))}
    ]).

rewrite(deleted) -> <<0>>;
rewrite(Bin) when is_binary(Bin) -> <<0, Bin/binary>>.

modified_entry(N, Range) ->
    {integer_to_binary(N), version(Range), value()}.
