-module(prop_shunk_tab_serv).
-include_lib("proper/include/proper.hrl").
-define(PAD_SIZE, 10000).

prop_diff() ->
    ?FORALL({EntriesA, EntriesB}, entries(),
            begin
                {ok,A} = shunk_tab_serv:start_link(a),
                {ok,B} = shunk_tab_serv:start_link(b),
                Pad = pad(?PAD_SIZE),
                AllA = Pad ++ EntriesA,
                AllB = Pad ++ EntriesB,
                fill_tab(a, AllA),
                fill_tab(b, AllB),
                {ok, VA, LA} = shunk_tab_serv:levels(a, infinity),
                {ok, VB, LB} = shunk_tab_serv:levels(b, infinity),
                Ranges = shunk_snap:diff_lvls(LA, LB),
                CAllA = current_entries(AllA),
                CAllB = current_entries(AllB),
                Diff =  (CAllA -- CAllB) ++ (CAllB -- CAllA),
                case in_range(Diff, Ranges) of
                    true ->
                        ok;
                    false ->
                        io:format("~p~n~p~n", [current_entries(EntriesA -- EntriesB),
                                               current_entries(EntriesB -- EntriesA)]),
                        io:format("~p~n", [{Diff, Ranges}])
                        %error({Diff, Ranges})
                end,
                TDiff = lists:flatten([begin
                                         MA = shunk_tab_serv:sublevels(a, VA, Range),
                                         MB = shunk_tab_serv:sublevels(b, VB, Range),
                                         %% TODO: if the range doesn't exist locally, the
                                         %% diff omits them.
                                         [Min || {Min,Min} <- shunk_snap:diff_lvls(MA,MB)]
                                       end || Range <- Ranges]),
                shunk_tab_serv:return_vsn(a, VA),
                shunk_tab_serv:return_vsn(b, VB),
                unlink(A),
                unlink(B),
                gen_server:stop(A),
                gen_server:stop(B),
                ?WHENFAIL(
                   (io:format("Diff: ~p~nTDiff: ~p~n~p~n~p~n",
                              [lists:usort([element(1,X) || X <- Diff]),
                               TDiff, {AllA, AllB},
                               {current_entries(AllA),current_entries(AllB)}])),
                   (lists:usort([element(1,X) || X <- Diff]) =:= TDiff)
                )
            end).

prop_multi_chunk() ->
    %% Compare that a full rehash is the same as sequential partial ones
    %% -- we do work with a whitebox approach here.
    %% Could make assumptions about time durations.
    ?FORALL(EntriesList, rand_seq(),
            begin
                {ok,P} = shunk_tab_serv:start_link(partial),
                {ok,F} = shunk_tab_serv:start_link(final),
                Pad = pad(?PAD_SIZE),
                fill_tab(final, Pad ++ lists:append(EntriesList)),
                {ok, _, LF} = shunk_tab_serv:levels(final, 10000),
                fill_tab(partial, Pad),
                [begin
                     fill_tab(partial, Entries),
                     shunk_tab_serv:levels(partial, 10000)
                 end || Entries <- EntriesList],
                {ok, _, LP} = shunk_tab_serv:levels(partial, 10000),
                unlink(P),
                unlink(F),
                gen_server:stop(P),
                gen_server:stop(F),
                LF =:= LP
            end).


%%% HELPERS
fill_tab(Name, Entries) ->
    [case V of
         deleted -> shunk_tab_serv:delete(Name, K);
         _ -> shunk_tab_serv:insert(Name, K, V)
     end || {K,V} <- Entries],
    ok.

in_range([], _) -> true;
in_range([{K,_}|T], Ranges) ->
    key_in_range(K, Ranges)
    andalso
    in_range(T, Ranges).

key_in_range(_, []) -> false;
key_in_range(K, [{A,B}|_]) when K >= A, K =< B -> true;
key_in_range(K, [_|T]) -> key_in_range(K, T).

current_entries(List) ->
    lists:foldl(
      %% Get rid of deleted values in tail position
      fun({K,V}, [{K,_}|Acc]) ->
              case V of
                  deleted -> Acc;
                  _ -> [{K,V}|Acc]
              end;
         ({K,V}, Acc) ->
              case V of
                  deleted -> Acc;
                  _ -> [{K,V}|Acc]
              end
      end,
      [],
      %% Need a stable sort to ensure overwrites are not crushed.
      %% There is a need for the version to be considered too, but
      %% that isn't really doable here without a custom sorting
      %% algorithm.
      lists:keysort(1,List)).

%%% GENERATORS %%%

rand_seq() ->
    non_empty([
        non_empty([{non_empty(binary()), value()}])
    ]).

entries() ->
    entries({0, [], []}).

pad(0) -> [];
pad(N) -> [{integer_to_binary(N), <<"pad">>} | pad(N-1)].

entries({N, A, B}) ->
    weighted_union([
        %% end
        {1, {A, B}},
        %% add to both
        {20, ?LET(Entry, entry(N),
                  ?LAZY(entries({N+1, [Entry|A], [Entry|B]})))},
        %% add to A
        {5, ?LAZY(entries({N+1, [entry(N)|A], B}))},
        %% add to B
        {5, ?LAZY(entries({N+1, A, [entry(N)|B]}))}
    ]).

entry(N) ->
    {integer_to_binary(N), value()}.

value() ->
    weighted_union([
        {1, deleted},
        {40, non_empty(binary())}
    ]).
