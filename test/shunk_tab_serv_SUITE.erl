-module(shunk_tab_serv_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").
-compile(export_all).
-define(DIRTY_OFFSET, 4). % position of dirty table in record

all() -> [simple_insert_levels, inf_loop, chunk_dirty_gc, manual_gc].

init_per_testcase(_, Config) ->
    {ok, Pid} = shunk_tab_serv:start_link(?MODULE),
    [{name,?MODULE},{pid,Pid}|Config].

end_per_testcase(_, Config) ->
    Pid = ?config(pid, Config),
    unlink(Pid),
    gen_server:stop(Pid).

simple_insert_levels(Config) ->
    Name = ?config(name, Config),
    shunk_tab_serv:insert(Name, <<"key">>, <<"val">>),
    {ok, {Ref1,Vsn1}, Lvls1} = shunk_tab_serv:levels(Name, 1000),
    shunk_tab_serv:insert(Name, <<"key2">>, <<"val">>),
    {ok, {Ref2,Vsn2}, Lvls2} = shunk_tab_serv:levels(Name, 1000),
    ?assert(Ref1 =/= Ref2),
    ?assert(Vsn1 < Vsn2), % had a write in between
    ?assertMatch([{0,[{<<"key">>,<<"key">>, _Hash, 1}]}], Lvls1),
    ?assertMatch([{0,[{<<"key">>,<<"key2">>, _Hash, 2}]}], Lvls2),
    ok.

inf_loop(Config) ->
    Name = ?config(name, Config),
    shunk_tab_serv:insert(Name, <<"key">>, <<"val">>),
    shunk_tab_serv:delete(Name, <<"otherkey">>),
    Parent = self(),
    spawn_link(fun() -> Parent ! shunk_tab_serv:levels(Name, 1000) end),
    receive
        {ok, {_Ref,_Vsn}, _Lvls} -> ok
    end.

chunk_dirty_gc(Config) ->
    ct:pal("position of dirty table is assumed to be ~p", [?DIRTY_OFFSET]),
    Name = ?config(name, Config),
    DirtyTid = element(?DIRTY_OFFSET, sys:get_state(Name)),
    shunk_tab_serv:insert(Name, <<"key1">>, <<"val1">>),
    shunk_tab_serv:insert(Name, <<"key2">>, <<"val1">>),
    shunk_tab_serv:lookup(Name, <<"key1">>),
    shunk_tab_serv:insert(Name, <<"key1">>, <<"val2">>),
    ?assertEqual([{{1,<<"key1">>}, dirty},
                  {{1,<<"key2">>}, dirty},
                  {{2,<<"key1">>}, dirty}],
                 ets:tab2list(DirtyTid)),
    shunk_tab_serv:levels(Name, 500), % first GC is synchronous
    ?assertEqual([], ets:tab2list(DirtyTid)),
    shunk_tab_serv:insert(Name, <<"key1">>, <<"val1">>),
    shunk_tab_serv:insert(Name, <<"key2">>, <<"val1">>),
    shunk_tab_serv:lookup(Name, <<"key1">>),
    ?assertEqual([{{3,<<"key1">>}, dirty},
                  {{3,<<"key2">>}, dirty}],
                 ets:tab2list(DirtyTid)),
    shunk_tab_serv:levels(Name, 500), % subsequebt GCs are asynchronous
    timer:sleep(500),
    ?assertEqual([], ets:tab2list(DirtyTid)),
    ok.

manual_gc(Config) ->
    Name = ?config(name, Config),
    %% Have a reader holding things up at vsn 1
    VsnRef = shunk_tab_serv:read_vsn(Name),
    %% below starts writing at vsn 2, since a read lock is taken
    shunk_tab_serv:insert(Name, <<"key1">>, <<"val1">>),
    %% Bump to version 3 with another lookup, meaning 2 is GC-able if no
    %% read lock is present (we still have lock at 1)
    shunk_tab_serv:lookup(Name, <<"key1">>),
    shunk_tab_serv:insert(Name, <<"key1">>, <<"val2">>),
    %% Running a GC leaves the entry at version 2 clear since 1 is locked
    ?assertMatch([{{<<"key1">>,2},_}], ets:lookup(Name, {<<"key1">>,2})),
    shunk_tab_serv:run_gc(Name),
    ?assertMatch([{{<<"key1">>,2},_}], ets:lookup(Name, {<<"key1">>,2})),
    %% Return the VSN and now it's free to GC below vsn 3 since no locks are in
    shunk_tab_serv:return_vsn(Name, VsnRef),
    shunk_tab_serv:run_gc(Name),
    ?assertEqual([], ets:lookup(Name, {<<"key1">>,2})),
    ?assertMatch([{{<<"key1">>,3},_}], ets:lookup(Name, {<<"key1">>,3})),
    ok.


