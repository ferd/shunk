%% Modification stability is too hard to test with properties, let's trust
%% the paper.
%%
%% See:
%% If an insertion takes place outside of the sliding window boundaries for a
%% given chunk, and the chunk isn't at max size yet, then the write will
%% either:
%% - only grow that one, leave others unchanged;
%% - be the new cutoff point for that one
%%
%% If an insertion takes place within the sliding window boundary of a chunk,
%% or within a chunk that is at high threshold:
%% - expand the window by one (if hashes still match) ? maybe not
%% - be the new cutoff point for that chunk
%% - force the boundary-1 entry to be the new cutoff point for that chunk
%%
%% If a deletion happens on any chunk at any space outside of sliding window
%% boundaries:
%% - only shrink that one, leave others unchanged;
%% - create a new cutoff point
%%
%% If a deletion happens on any chunk within the sliding window boundary:
%% - only shrink that one (on hash equivalence)
%% - create a new cutoff point for that hash
%% - expand the window by changing rolling window, create alternative
%%   cutoff point
%%
%% A new cutoff point has the following impact:
%% - expand the followup window, but not more if the slice is
%%   within the low threshold; as long as the sliding window
%%   is smaller than the cutoff point, we should have stability
%%    -- although that may be false if a prior low threshold
%%    hid a proper match.
%% - an exception is if the chunk was at max size, in which
%%   case things will get shifted again on the next block
%%   by creating a new cutoff point
%%
%% The best we can probably do is hope for a probabilistically
%% better chunking than alternatives.
-module(prop_shunk_chunk).
-include_lib("proper/include/proper.hrl").
-export([iterate/2]). % useful in the shell to debug

-define(window_size, 15).
-define(threshold_low, 460).
-define(threshold_high, 2800).
-define(d1, 540).
-define(d2, 270).

%% @doc All thresholds for a given section of chunks is within the bounds
%% of `[Low, High]', with the exception of the last chunk, which may be
%% shorter since the data set size may not be sufficient to fill the
%% minimum threshold.
prop_thresholds() ->
    ?FORALL({{Low, High}, Bins}, {thresholds(), binaries()},
            begin
                State = shunk_chunk:init(?window_size, Low, High, ?d1, ?d2),
                [Last={_,LastN}|Chunks] = lists:reverse(iterate(Bins, State)),
                ?WHENFAIL(io:format("Low: ~p~nHigh: ~p~nChunks:~n~p~n",
                                    [Low, High, lists:reverse([Last|Chunks])]),
                    lists:all(fun({_Chunk,N}) -> N >= Low andalso N =< High end, Chunks)
                    andalso
                    %% Last can be smaller than the low threshold if there's no data left
                    LastN =< High
                )
            end).

%% @doc All thresholds account for the total of all entries
prop_chunk_sums() ->
    ?FORALL({{Low, High}, Bins}, {thresholds(), binaries()},
            begin
                State = shunk_chunk:init(?window_size, Low, High, ?d1, ?d2),
                Chunks = iterate(Bins, State),
                length(Bins) =:= lists:sum([N || {_,N} <- Chunks])
            end).

%%%%%%%%%%%%%%%
%%% HELPERS %%%
%%%%%%%%%%%%%%%
iterate(Bins, State) ->
    lists:reverse(iterate(list_to_tuple(Bins), State, 1, 1, [])).

iterate(Bins, _State, Pos, N, Chunks) when Pos =:= tuple_size(Bins) ->
    [{element(Pos, Bins), N} | Chunks];
iterate(Bins, State, Pos, N, Chunks) ->
    case shunk_chunk:chunk(element(Pos, Bins), State) of
        {next, NewState} ->
            iterate(Bins, NewState, Pos+1, N+1, Chunks);
        {chunk, Offset, Bin, NewState} ->
            iterate(Bins, NewState, (Pos+Offset)+1, 1, [{Bin, N+Offset}|Chunks])
    end.

%%%%%%%%%%%%%%%%%%
%%% GENERATORS %%%
%%%%%%%%%%%%%%%%%%
thresholds() ->
    ?SUCHTHAT({X,Y}, {pos_integer(), pos_integer()}, X < Y).

binaries() ->
    non_empty(
      ?SIZED(Size,
        resize(Size*100, list(non_empty(binary())))
      )
    ).
