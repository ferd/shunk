%%% @doc Server for local storage and handling of MVCC data with shunk.
%%% Is in charge of scheduling garbage collection, writes, and so on.
%%%
%%% As a note on the MVCC implementation, we only have lookups bump
%%% up the version of records in the table; because shunk is intended
%%% as a tool to synchronize data and not a general MVCC store, we make
%%% the assumption that reads are rarer than writes in such a system,
%%% as most writes will usually be streamed through and reads would
%%% then be used when debugging or repairing data.
-module(shunk_tab_serv).
-behaviour(gen_server).

%% API exports
-export([start_link/1, start_link/3,
         lookup/2, lookup_many/2, insert/3, insert/4, delete/2, delete/3,
         levels/2, sublevels/3]).
%% Internal building block exports
-export([read_vsn/1, return_vsn/2, run_gc/1]).
%% OTP Callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).

-record(state, {readers :: ets:tid(),
                tab :: ets:tid(),
                dirty :: ets:tid(),
                mode=write :: read | write,
                chunks=[] :: [term()],
                lvls=[] :: [term()],
                latest_chunks_vsn=undefined,
                vsn=1 :: pos_integer(),
                window_size :: pos_integer(),
                chunk_size :: pos_integer()}).

-opaque ref() :: {pid(), reference()}.
-opaque vsn_ref() :: {ref(), non_neg_integer()}.
-export_type([ref/0, vsn_ref/0]).
% we use atoms as names and tids at once
-dialyzer({nowarn_function, [lookup/2, lookup_many/2]}).

%%%%%%%%%%%
%%% API %%%
%%%%%%%%%%%

%% @doc equivalent to `start_link(Name, 5, 15)'
-spec start_link(atom()) -> {ok, pid()}.
start_link(Name) ->
    start_link(Name, 5, 15).

%% @doc boots a new shunk tab server
-spec start_link(Name, WindowSize, ChunkSize) -> {ok, pid()} when
      Name :: atom(),
      WindowSize :: pos_integer(),
      ChunkSize :: pos_integer().
start_link(Name, WS, CS) ->
    gen_server:start_link({local, Name}, ?MODULE, {Name, WS, CS}, []).

%% @doc looks up the latest value for a given key.
-spec lookup(term(), binary()) -> {ok, binary()} | {error, not_found}.
lookup(Name, Key) ->
    Ref = {_, Vsn} = read_vsn(Name),
    Res = shunk_tab:lookup(Name, Vsn, Key),
    return_vsn(Name, Ref),
    Res.

%% @doc looks up the latest value for a given list of keys. The versions
%% read are going to be consistent across all keys. This means that if,
%% for example, half the keys are modified at the same time we read,
%% these changes will be omitted and we'll get a set of results consistent
%% with the state of things when reads started.
-spec lookup_many(term(), [Key]) ->
    [{Key, {ok, binary()} | {error, not_found}}] when
    Key :: binary().
lookup_many(Name, Keys) when is_list(Keys) ->
    Ref = {_, Vsn} = read_vsn(Name),
    Res = [shunk_tab:lookup(Name, Vsn, Key) || Key <- Keys],
    return_vsn(Name, Ref),
    Res.

%% @doc Inserts a new key/value pair in the table. Equivalent to
%% `insert(Name, Key, Val, 5000)'.
-spec insert(atom(), binary(), binary()) -> true.
insert(Name, Key, Val) ->
    insert(Name, Key, Val, 5000).

%% @doc Inserts a new key/value pair in the table.
%% TODO: decide whether we accept arbitrary values and convert them, since
%% internals assume hashed values are used for easy comparisons
-spec insert(atom(), binary(), binary(), timeout()) -> true.
insert(Name, Key, Val, Timeout) ->
    gen_server:call(Name, {insert, Key, Val}, Timeout).

%% @doc Removes a given key and its associated value from the table.
%% `delete(Name, Key, 5000)'.
-spec delete(atom(), binary()) -> true.
delete(Name, Key) ->
    delete(Name, Key, 5000).

%% @doc Removes a given key and its associated value from the table.
-spec delete(atom(), binary(), timeout()) -> true.
delete(Name, Key, Timeout) ->
    gen_server:call(Name, {delete, Key}, Timeout).

%% returns the latest chunks. You can't choose a version for these
%% as the table only maintains the latest copy. Instead, a chunk
%% version will be returned, which can be used to access specific copies
%% of data.
-spec levels(atom(), timeout()) ->  {ok, vsn_ref(), term()}  | no_data.
levels(Name, Timeout) ->
    gen_server:call(Name, {levels, self()}, Timeout).

%% returns the latest chunks. You can't choose a version for these
%% as the table only maintains the latest copy. Instead, a chunk
%% version will be returned, which can be used to access specific copies
%% of data.
-spec sublevels(ets:tid(), vsn_ref(), shunk_snap:range()) -> [shunk_tab:lvl()].
sublevels(Name, _VsnRef={_Id,Vsn}, Range) ->
    shunk_snap:to_mlevels(Name, Vsn, Range).

%%% Internal calls?
-spec read_vsn(atom()) -> ref().
read_vsn(Name) ->
    gen_server:call(Name, {read_vsn, self()}).

-spec return_vsn(atom(), ref()) -> ok.
return_vsn(Name, Id) ->
    gen_server:cast(Name, {vsn, Id}).

-spec run_gc(atom()) -> ok.
run_gc(Name) ->
    gen_server:call(Name, manual_gc, infinity).

%%%%%%%%%%%%%%%%%
%%% CALLBACKS %%%
%%%%%%%%%%%%%%%%%
init({Name, WS, CS}) ->
    shunk_tab:new(Name),
    Readers = ets:new(readers, [protected, ordered_set]),
    Dirty = ets:new(dirty, [protected, ordered_set]),
    {ok, #state{readers=Readers, tab=Name, dirty=Dirty,
                window_size=WS, chunk_size=CS}}.

handle_call({read_vsn,Reader}, _From, S=#state{mode=Mode, vsn=OldVsn, readers=Readers}) ->
    %% For efficiency reason of a use case like shunk -- continuous updates and
    %% once-in-a-while diffing, we cheat on MVCC by only incrementing the counter
    %% on reads (which should be rarer than writes) and then returning an incremented
    %% value for the next write mode, allowing writes to continue unhindered as long
    %% as they have their ID.
    Vsn = case Mode of
              read -> OldVsn;
              write -> OldVsn+1
          end,
    Id = add_reader(Reader, OldVsn, Readers),
    {reply, {Id, OldVsn}, S#state{mode=read, vsn=Vsn}};
handle_call({insert,K,V}, From, S=#state{mode=read}) ->
    handle_call({insert,K,V}, From, S#state{mode=write});
handle_call({delete,K}, From, S=#state{mode=read}) ->
    handle_call({delete,K}, From, S#state{mode=write});
handle_call({insert,K,V}, _From, State=#state{mode=write, vsn=Vsn, tab=T, dirty=D}) ->
    shunk_tab:insert(T, K, V, Vsn),
    mark_dirty(D, Vsn, K),
    {reply, ok, State};
handle_call({delete,K}, _From, State=#state{mode=write, vsn=Vsn, tab=T, dirty=D}) ->
    shunk_tab:delete(T, K, Vsn),
    mark_dirty(D, Vsn, K),
    {reply, ok, State};
handle_call({levels,Reader}, _From, State=#state{latest_chunks_vsn=undefined, mode=Mode,
                                                 tab=T, vsn=OldVsn, readers=Readers, dirty=D,
                                                 window_size=WS, chunk_size=CS}) ->
    case ets:info(T, size) of
        0 -> throw({reply, no_data, State});
        _ -> ok
    end,
    Vsn = case Mode of
              read -> OldVsn;
              write -> OldVsn+1
          end,
    Id = add_reader(Reader, OldVsn, Readers),
    %% First chunking is synchronous because why not
    Chunks = shunk_tab:chunk(T, OldVsn, WS, CS),
    Lvls = shunk_snap:to_levels(T, OldVsn, Chunks),
    gc_dirty(D, OldVsn),
    {reply, {ok, {Id,OldVsn}, Lvls},
     State#state{latest_chunks_vsn=Vsn, chunks=Chunks, lvls=Lvls, vsn=Vsn}};
handle_call({levels,Reader}, From, State=#state{mode=Mode, vsn=OldVsn, tab=T,
                                                dirty=D, chunks=C, lvls=Lvls,
                                                window_size=WS, chunk_size=CS,
                                                readers=Readers}) ->
    %% To help with resources, we bump the version of chunks if in write mode
    %% to avoid remodifying older chunks, which means emptying the dirty table,
    %% and so on.
    %% If in read mode, we can keep the same version but still do the same
    %% otherwise.
    %% TODO: find a way to make this concurrent rather than blocking?
    Vsn = case Mode of
              read -> OldVsn;
              write -> OldVsn+1
          end,
    %% we hand off that ID to the client for usage at the rest of a diff
    Id = add_reader(Reader, Vsn, Readers),
    Dirty = shunk_tab:mark_dirty(C, find_dirty(D, OldVsn)),
    Server = self(),
    %% TODO: optimize if no dirty entries are found
    spawn_link(fun() ->
        %% We could split up from here since writes are done, but report back
        %% here for chunks of that version.
        PChunks = shunk_tab:partial_chunk(T, OldVsn, Dirty, WS, CS),
        Lvls2 = shunk_snap:partial_to_levels(T, OldVsn, PChunks, lists:last(Lvls)),
        gen_server:reply(From, {ok, {Id,OldVsn}, Lvls2}),
        gen_server:cast(Server, {levels, OldVsn,
                                 %% deannotate chunks
                                 [element(2,P) || P <- PChunks],
                                 Lvls2})
    end),
    {noreply, State#state{vsn=Vsn}};
handle_call(manual_gc, _From, S=#state{readers=Readers, tab=T, vsn=CurrentVsn}) ->
    Vsn = oldest_reader_vsn(Readers, CurrentVsn),
    shunk_tab:gc(T, Vsn),
    {reply, ok, S}.


handle_cast({vsn, {Id,_Vsn}}, S=#state{readers=Readers}) ->
    remove_reader(Id, Readers),
    {noreply, S};
handle_cast({levels, ChunkVsn, Chunks, Lvls}, S=#state{latest_chunks_vsn=LCVsn, dirty=D}) ->
    if ChunkVsn > LCVsn; LCVsn =:= undefiend ->
           gc_dirty(D, ChunkVsn),
           {noreply, S#state{latest_chunks_vsn=ChunkVsn, chunks=Chunks, lvls=Lvls}};
       ChunkVsn =< LCVsn ->
           {noreply, S}
    end.

handle_info({'DOWN', Ref, process, Pid, _}, S=#state{readers=Readers}) ->
    remove_reader({Pid, Ref}, Readers),
    {noreply, S}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

terminate(_, _) ->
    ok.

%%%%%%%%%%%%%%%
%%% HELPERS %%%
%%%%%%%%%%%%%%%
add_reader(Pid, Vsn, Tid) ->
    Ref = erlang:monitor(process, Pid),
    ets:insert(Tid, {{Pid,Ref},Vsn}),
    {Pid,Ref}.

remove_reader({Pid,Ref}, Tid) when is_pid(Pid), is_reference(Ref) ->
    erlang:demonitor(Ref, [flush]),
    ets:delete(Tid, {Pid,Ref}).

oldest_reader_vsn(Readers, Current) ->
    ets:foldl(fun({_, Vsn}, OldVsn) -> min(Vsn,OldVsn) end, Current, Readers).

mark_dirty(Tid, Vsn, Key) ->
    ets:insert(Tid, {{Vsn,Key}, dirty}).

find_dirty(Tid, Vsn) ->
    ets:select(Tid, [{{{'$1','$2'},'_'}, [{'=<','$1',Vsn}], ['$2']}]).

gc_dirty(Tid, Vsn) ->
    ets:select_delete(Tid, [{{{'$1','_'},'_'}, [{'=<','$1',Vsn}], [true]}]).

