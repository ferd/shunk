%%% @doc Work chunkable tables with content as MVCC values.
%% The objective is to be used in MVCC mode where writes are private and
%% serialized but reads are public, while usable for diffs.
%%
%% This works with the assumption that diffs read up to a given
%% write version for stability and something will move on to trigger
%% garbage collections.
%%
%% Whenever a diff or a write takes place, increment the version for
%% writes. Writes can otherwise be batched at once since they are
%% assumed to happen more often.
-module(shunk_tab).
-export([new/1, lookup/3, insert/4, delete/3,
         chunk/2, chunk/4, chunk/7, partial_chunk/3, partial_chunk/5,
         partial_chunk/8, mark_dirty/2,
         first/2, last/2, next/3, fold/6,
         gc/2]).
%% functions added for symmetry or for external users, and otherwise
%% not used internally except maybe for tests.
-ignore_xref([chunk/2, chunk/7, partial_chunk/3, partial_chunk/8,
              last/2, next/3]).

-type chunk() :: {Start::binary(), End::binary(), Count::pos_integer()}.
-type annotated_chunk() :: {dirty | clean, chunk()}.
-export_type([chunk/0, annotated_chunk/0]).

%%%%%%%%%%%%%%
%%% PUBLIC %%%
%%%%%%%%%%%%%%

%% @doc Open up a new table. Tables are named and protected with the
%% objective to be used in MVCC mode where writes are private and
%% serialized but reads are public.
-spec new(atom()) -> ets:tid().
new(Name) ->
    ets:new(Name, [named_table, protected, ordered_set, {read_concurrency, true}]).

%% @doc Write an entry at a given version `Vsn'.
-spec insert(ets:tid(), binary(), binary(), pos_integer()) -> true.
insert(T, Key, Val, Vsn) ->
    ets:insert(T, {{Key,Vsn}, {value,Val}}).

%% @doc Mark an entry at a given version as deleted. Because of MVCC,
%% the entries cannot be fully flushed without risking a reversal.
%% Garbage collection will need to clear things up.
-spec delete(ets:tid(), binary(), pos_integer()) -> true.
delete(T, Key, Vsn) ->
    ets:insert(T, {{Key,Vsn}, deleted}).

%% @doc Find the value for a given `Key' current at version `MaxVsn'
-spec lookup(ets:tid(), pos_integer(), binary()) ->
    {ok, binary()} | {error, not_found}.
lookup(T, MaxVsn, Key) ->
    case ets:prev(T, {Key, MaxVsn+1}) of
        {Key, Vsn} -> % same key
            case ets:lookup(T, {Key,Vsn}) of
                [{{Key,Vsn}, {value,Val}}] -> {ok, Val};
                _Other -> {error, not_found}
            end;
        _Other ->
            {error, not_found}
    end.

%% @doc Equivalent to `chunk(T, Vsn, 15, 460, 2800, 540, 270)'.
-spec chunk(ets:tid(), pos_integer()) -> [chunk()].
chunk(T, Vsn) ->
    State = shunk_chunk:init(),
    First = first(T, Vsn),
    lists:reverse(chunk_(T, Vsn, First, First, State, 1, [])).

%% @doc Fairly scales chunks to fit a given window size.
-spec chunk(ets:tid(), pos_integer(), pos_integer(), pos_integer()) -> [chunk()].
chunk(T, Vsn, WindowSize, ChunkSize) ->
    State = shunk_chunk:init(WindowSize, ChunkSize),
    First = first(T, Vsn),
    lists:reverse(chunk_(T, Vsn, First, First, State, 1, [])).

%% @doc Chunk the contents of the table based on the values it contains.
%% The chunks should provide some level of stability even when entries
%% get modified, added, or removed.
%% @see shunk_chunk:init/5
-spec chunk(T, Vsn, WindowSize, Low, High, D1, D2) -> [chunk()] when
      T :: ets:tid(),
      Vsn :: pos_integer(),
      WindowSize :: pos_integer(),
      Low :: pos_integer(),
      High :: pos_integer(),
      D1 :: non_neg_integer(),
      D2 :: non_neg_integer().
chunk(T, Vsn, WindowSize, Low, High, D1, D2) ->
    State = shunk_chunk:init(WindowSize, Low, High, D1, D2),
    First = first(T, Vsn),
    lists:reverse(chunk_(T, Vsn, First, First, State, 1, [])).

%% @doc Equivalent to `partial_chunk(T, Vsn, Chunks, 15, 460, 2800, 540, 270)'.
-spec partial_chunk(ets:tid(), pos_integer(), [annotated_chunk()]) -> [annotated_chunk()].
partial_chunk(T, Vsn, Chunks) ->
    State = shunk_chunk:init(),
    partial_clean(T, Vsn, first(T, Vsn), State, Chunks).

partial_chunk(T, Vsn, Chunks, WindowSize, ChunkSize) ->
    State = shunk_chunk:init(WindowSize, ChunkSize),
    partial_clean(T, Vsn, first(T, Vsn), State, Chunks).

%% @doc Chunk the contents of the table based on the values it contains.
%% The chunks should provide some level of stability even when entries
%% get modified, added, or removed.
%% @see shunk_chunk:init/5
-spec partial_chunk(T, Vsn, [annotated_chunk()], WindowSize, Low, High, D1, D2) ->
    [annotated_chunk()] when
      T :: ets:tid(),
      Vsn :: pos_integer(),
      WindowSize :: pos_integer(),
      Low :: pos_integer(),
      High :: pos_integer(),
      D1 :: non_neg_integer(),
      D2 :: non_neg_integer().
partial_chunk(T, Vsn, Chunks, WindowSize, Low, High, D1, D2) ->
    State = shunk_chunk:init(WindowSize, Low, High, D1, D2),
    partial_clean(T, Vsn, first(T, Vsn), State, Chunks).

%% @doc Takes a list of previously calculated `Chunks' and a list of
%% keys that have been rewritten since then, and returns a list of
%% chunks individually tagged as either `clean' or `dirty'. Such
%% as list of chunks can then be passed to `partial_chunk/3-8' to
%% recalculate the new chunk values without incurring the cost of
%% a full table chunking.
-spec mark_dirty([chunk()], [Key]) -> [{Type,chunk()}] when
      Type :: clean | dirty,
      Key :: binary().
mark_dirty(Chunks, ModifiedKeys) ->
    mark_dirty_(lists:sort(ModifiedKeys),
                [{clean, X} || X <- Chunks]).

%% @doc Runs a fold operation between keys `[Start..End]' at a
%% given `Vsn' in order.
-spec fold(ets:tid(), MaxVsn, Start, End, Fun, FunState) -> FunState when
      MaxVsn :: pos_integer(),
      Start :: Key,
      End :: Key,
      Key :: binary(),
      Val :: binary(),
      Fun :: fun((Key, Val, FunState) -> FunState),
      FunState :: term().
fold(T, MaxVsn, Start, End, Fun, State) ->
    case ets:prev(T, {Start, MaxVsn+1}) of
        {Start, Vsn} ->
            fold_iter(T, MaxVsn, {Start,Vsn}, End, Fun, State);
        '$end_of_table' ->
            %% not there, fold from later!
            case first(T, MaxVsn) of
                {NewStart,NewVsn} when NewStart =< End ->
                    fold_iter(T, MaxVsn, {NewStart,NewVsn}, End, Fun, State);
                _ -> % no overlapping range
                    []
            end;
        _Other ->
            error({start_not_found, Start})
    end.

%% @doc Equivalent to `ets:first/1' but skipping newer versions and
%% entries marked as deleted.
-spec first(ets:tid(), Vsn) -> {Key, Vsn} | '$end_of_table' when
      Key :: binary(),
      Vsn :: pos_integer().
first(T, MaxVsn) ->
    case ets:first(T) of
        {K,Vsn} when Vsn =< MaxVsn ->
            maybe_skip_deleted_next(T, MaxVsn, K);
        {K,_Newer} ->
            next(T, MaxVsn, K);
        '$end_of_table' ->
            '$end_of_table'
    end.

%% @doc Equivalent to `ets:last/1' but skipping newer versions and
%% entries marked as deleted.
-spec last(ets:tid(), Vsn) -> {Key, Vsn} | '$end_of_table' when
      Key :: binary(),
      Vsn :: pos_integer().
last(T, MaxVsn) ->
    case ets:last(T) of
        {K,Vsn} when Vsn =< MaxVsn ->
            maybe_skip_deleted_prev(T, MaxVsn, K, Vsn);
        {K,Newer} ->
            last_prev(T, MaxVsn, K, Newer);
        '$end_of_table' ->
            '$end_of_table'
    end.

%% @doc Equivalent to `ets:next/2' but skipping newer versions and
%% entries marked as deleted.
-spec next(ets:tid(), Vsn, Key) -> {Key, Vsn} | '$end_of_table' when
      Key :: binary(),
      Vsn :: pos_integer().
next(T, MaxVsn, Key) ->
    case ets:next(T, {Key, MaxVsn}) of
        {Key, Vsn} when Vsn > MaxVsn ->
            next_skipahead(T, MaxVsn, Key, Vsn);
        {NewKey, Vsn} when Vsn =< MaxVsn ->
            maybe_skip_deleted_next(T, MaxVsn, NewKey);
        {NewKey, Vsn} when Vsn > MaxVsn ->
            next(T, MaxVsn, NewKey);
        '$end_of_table' ->
            '$end_of_table'
    end.

%% @doc Garbage collect outdated and stale MVCC data.
%%
%% The flushes all data older than the minimal version passed
%% to the function.
%% This also finds all keys where all versions are marked as
%% deleted, and removes them, since no possible version in MVCC could
%% see it as read. Technically the latter pass is stricter and also
%% GCs all deletions before a readable write (if any).
-spec gc(ets:tid(), pos_integer()) -> ok.
gc(T, Vsn) ->
    case ets:first(T) of
        '$end_of_table' -> ok;
        First = {Key,_Vsn} -> gc(T, Vsn, First, Key, [], [], del)
    end.

%%%%%%%%%%%%%%%
%%% PRIVATE %%%
%%%%%%%%%%%%%%%

chunk_(_T, _Vsn, '$end_of_table', '$end_of_table', _State, _N, Chunks) ->
    Chunks;
chunk_(T, Vsn, '$end_of_table', {FirstKey,_}, _State, N, Chunks) ->
    {Last, _Vsn} = last(T, Vsn),
    case Chunks of
        [{_, Last,_}|_] -> % covered all entries
            Chunks;
        _ -> % pick the last value as a chunk boundary
            [{FirstKey, Last,N-1}|Chunks]
    end;
chunk_(T, Vsn, KeyVsn={Key,_}, First, State, N, Chunks) ->
    [{_, {value,V}}] = ets:lookup(T, KeyVsn), % value *has* to be there
    %% Chunk based on key + value contents
    case shunk_chunk:chunk([Key,V], State) of
        {chunk, Offset, [KChunk,_], NewState} ->
            Next = next(T, Vsn, KChunk),
            {FirstKey,_} = First,
            NewChunks = [{FirstKey, KChunk,N+Offset}|Chunks],
            case Next of
                '$end_of_table' ->
                    NewChunks;
                _ ->
                    chunk_(T, Vsn, Next, Next, NewState, 1, NewChunks)
            end;
        {next, NewState} ->
            chunk_(T, Vsn, next(T, Vsn, Key), First, NewState, N+1, Chunks)
    end.


%% TODO: REFACTOR THIS NIGHTMARE
partial_clean(_T, _Vsn, '$end_of_table', _State, []) ->
    [];
partial_clean(T, Vsn, Next={FirstChunkKey,_}, State, []) ->
    %% mandatory dirty, since we're not done and we're out of chunks
    partial_dirty(T, Vsn, Next, FirstChunkKey, State, 1, []);
partial_clean(T, Vsn, {Key,_}, State, [{clean, {Min,Max,_}}|_] = Chunks) when Key >= Min, Key =< Max ->
    {CleanRecs, CleanMax, NewChunks} = until_dirty(Chunks),
    CleanRecs ++ partial_clean(T, Vsn, next(T, Vsn, CleanMax), State, NewChunks);
partial_clean(T, Vsn, KeyVsn = {Key, _}, State, [{dirty, {Min,Max,_}}|_]=Chunks) when Key >= Min, Key =< Max ->
    %% we don't know if Min is still good.
    First = case lookup(T, Vsn, Min) of
        {ok, _} -> Min;
        {error, not_found} -> element(1,next(T, Vsn, Min))
    end,
    partial_dirty(T, Vsn, KeyVsn, First, State, 1, Chunks).

%% Double-check this; end of table, truncate all that's left
partial_dirty(_T, _Vsn, '$end_of_table', '$end_of_table', _State, _N, _Chunks) ->
    []; % empty table for next chunks; done
partial_dirty(T, Vsn, '$end_of_table', First, _State, N, _Chunks) ->
    case last(T, Vsn) of
        '$end_of_table' ->
            []; % empty table for this version; truncate
        {Last,_Vsn} ->
            if N =:= 1 -> % covered in submitted entries
                   [];
               N > 1 -> % pick the last value as a chunk boundary
                   [{dirty, {First, Last,N-1}}]
            end
    end;
partial_dirty(T, Vsn, KeyVsn={Key,_}, First, State, N, Chunks) ->
    [{_, {value,V}}] = ets:lookup(T, KeyVsn), % value *has* to be there
    %% Chunk based on key + value contents
    case shunk_chunk:chunk([Key,V], State) of
        {chunk, Offset, [KChunk,_], NewState} ->
            Next = next(T, Vsn, KChunk),
            NewChunk = {dirty,{First,KChunk,N+Offset}},
            {NextType, NextChunks} = next_type(KChunk, Chunks),
            [NewChunk |
             case NextType of
                 clean ->
                     partial_clean(T, Vsn, Next, NewState, NextChunks);
                 dirty ->
                     NextFirst = case Next of
                         {NextKey,_} -> NextKey;
                         '$end_of_table' -> '$end_of_table'
                     end,
                     partial_dirty(T, Vsn, Next, NextFirst, NewState, 1, NextChunks)
             end];
        {next, NewState} ->
            Next = next(T, Vsn, Key),
            partial_dirty(T, Vsn, Next, First, NewState, N+1, Chunks)
    end.

%% See `until_dirty/2'
until_dirty(Recs) -> until_dirty(Recs, []).

%% Skip ahead over all sequential clean records to avoid doing table lookups on
%% values we know are good and stable. As we skip the values, keep track of the
%% largest `Max' boundary of a clean record we have seen until we hit a dirty
%% one (or no records) as this is the only other lookup we need to do to keep
%% going.
%%
%% With this optimization, rather than paying the cost of `N+M' where `N' is the
%% number of clean chunks and `M' is the number of dirty entries, the cost is
%% simply `M' for the costly parts of reading the table. Given `N' can be very
%% large and `M' relatively small, this represents an important gain in such
%% scenarios, at nearly no extra cost for others.
until_dirty([], [{_,{_,Max,_}}|_]=Acc) ->
    {lists:reverse(Acc), Max, []};
until_dirty([{dirty,_}|_]=Recs, [{_,{_,Max,_}}|_]=Acc) ->
    {lists:reverse(Acc), Max, Recs};
until_dirty([{clean,_}=Rec|T], Acc) ->
    until_dirty(T, [Rec|Acc]).

%% we're finishing flush there; next chunk is possibly clean, but we let the
%% switching be fully defined by partial_clean.
next_type(KChunk, [{_,{_,KChunk,_}} | NextChunks]) ->
    {clean, NextChunks};
%% we're finishing early, start a new dirty chunk since we're offset from
%% whatever would be clean
next_type(KChunk, [{_,{_,ChunkKey,_}} | _]=Chunks) when KChunk < ChunkKey ->
    {dirty, Chunks};
%% special case: no chunks left! all dirty from here on!
next_type(_, []) ->
    {dirty, []};
%% we're finishing late; we must shed chunks until we arrive either fully
%% aligned or early from another chunk, since it means we're iterating over
%% the old chunk's content.
next_type(KChunk, [{_,{_,ChunkKey,_}} | Chunks]) when KChunk > ChunkKey ->
    next_type(KChunk, Chunks).

%% @private
%% Marks all written keys as dirty in their chunk intervals;
%% the last chunk is always considered dirty
-spec mark_dirty_([binary()], [{Type,chunk()}]) -> [{Type,chunk()}] when
      Type :: clean | dirty.
mark_dirty_([], []) ->
    %% If all is empty, there's nothing to do
    [];
mark_dirty_([], Rest) ->
    %% Trailing chunks; always mark the last one as dirty in case it
    %% was smaller than the smallest allowed chunk size; allows out
    %% of bound detection every time.
    Last = {_, LastVal} = lists:last(Rest),
    (Rest -- [Last]) ++ [{dirty, LastVal}];
mark_dirty_([K|_], [{_,{Min,Max,N}}]) ->
    %% Only last chunk left; always mark dirty
    %% We may however need to expand its range down.
    %% We don't need to expand up as the partiala rehashing
    %% algorithm should handle new trailing ranges being created.
    if K < Min ->
        [{dirty,{K,Max,N}}];
       K >= Min ->
        [{dirty,{Min,Max,N}}]
    end;
mark_dirty_([_|_], []) ->
    %% no more chunks to mark as dirty
    [];
mark_dirty_([K|Entries], [{Type,{Min,Max,N}}|Chunks]) ->
    if K >= Min, K =< Max -> % right in
           mark_dirty_(Entries, [{dirty,{Min,Max,N}}|Chunks]);
       K < Min -> % resize the chunk, mark as dirty
           mark_dirty_(Entries, [{dirty,{K,Max,N}}|Chunks]);
       K > Max -> % go forth
           [{Type,{Min,Max,N}} | mark_dirty_([K|Entries], Chunks)]
    end.


%% @private Helper for `next/3' with the purpose of skipping
%% forwards on keys if there's newer versions blocking us.
next_skipahead(T, MaxVsn, Key, Vsn) ->
    case ets:next(T, {Key, Vsn}) of
        {Key, NewVsn} -> % necessarily > MaxVsn
            next_skipahead(T, MaxVsn, Key, NewVsn);
        {NewKey, NewVsn} when NewVsn =< MaxVsn ->
            maybe_skip_deleted_next(T, MaxVsn, NewKey);
        {NewKey, NewVsn} when NewVsn > MaxVsn ->
            next_skipahead(T, MaxVsn, NewKey, NewVsn);
        '$end_of_table' ->
            '$end_of_table'
    end.

%% @private if the element is marked as deleted, skip
%% to the next key.
maybe_skip_deleted_next(T, MaxVsn, Key) ->
    {NextKey,NextVsn} = ets:prev(T, {Key, MaxVsn+1}),
    case is_deleted(T, NextKey, NextVsn) of
        true -> next_skipahead(T, MaxVsn, NextKey, NextVsn);
        false -> {NextKey, NextVsn}
    end.

%% @private Helper for `first/2' with the purpose of skipping
%% backwards on versions that are too new.
last_prev(T, MaxVsn, Key, Vsn) ->
    case ets:prev(T, {Key, Vsn}) of
        {NewKey, NewVsn} when NewVsn =< MaxVsn ->
            maybe_skip_deleted_prev(T, MaxVsn, NewKey, NewVsn);
        {NewKey, NewVsn} ->
            last_prev(T, MaxVsn, NewKey, NewVsn);
        '$end_of_table' ->
            '$end_of_table'
    end.


%% @private if the element is marked as deleted, skip
%% to the next key, backwards.
maybe_skip_deleted_prev(T, MaxVsn, Key, Vsn) ->
    case is_deleted(T, Key, Vsn) of
        true -> last_prev(T, MaxVsn, Key, 1);
        false -> {Key,Vsn}
    end.

%% @private check if the current element is deleted.
is_deleted(T, Key, Vsn) ->
    case ets:lookup(T, {Key,Vsn}) of
        [{_,deleted}] -> true;
        _ -> false
    end.

%% @private Do a single pass over the entire data set.
%% The GC has two components based on how datasets may exist:
%% ```
%%                              ,-- MinVsn
%%                             |
%%     |     1     2     3     4     5     6     7      8      9
%% ----|--------------------------------------------------------------
%% X   |     a     b
%% Y   |     a     -           b     c
%% Z   |     a           -
%% W   |     a     b                 c
%% R   |     a     -
%% S   |                 a     -     -     -     b
%% T   |                       a     -     -     b
%% U   |                       -     -     -
%% V   |                       -     -     a
%% '''
%% There's a divide for entries older than `MinVsn' and those newer.
%% If no entry exists past `MinVsn' but no record has been deleted
%% (as for `X'), then the value at `2' (`b') must be kept, but we can flush
%% older versions. In the case where the last version recorded is a delete
%% prior to `MinVsn', we can delete everything (as in `R').
%%
%% In cases where some entry exists at or past `MinVsn', those prior versions
%% can be garbage collected entirely (in `W', versions 1 and 2 can be flushed).
%%
%% Deleted entries in a sequence may be removed if no prior reachable entry is
%% in the list. This means that for `S', versions 3..6 may safely be removed,
%% but for `T', all of them have to be kept since versions 5-6 contradict the
%% value in version 4. In `U', all entries may be flushed, and in `V', only 4
%% and 5 can be flushed.
%%
%% This function accomplishes that by the usage of two lists: `Older' and
%% `CurrentDeleted'. `Older' keeps in a list of all values seen for a key that
%% are older than `MinVsn'. `CurrentDeleted' amasses sequences of deleted
%% versions greater than or equal to `MinVsn' (still reachable).
%%
%% Whenever a Current value is reached, we can flush the entire set of versions
%% in `Older'. If no Current value is ever reached, we may remove all entries
%% in `Older' iff the newest one is deleted; otherwise, we can remove all
%% entries in `Older' *except* the newest one.
%%
%% For `CurrentDeleted', the moment we see a value to be kept, all accumulated
%% deleted values are flushed; the run for the key switches from `del' state to
%% `keep', and all current values thereafter are not garbage collected. This
%% ensures that scenarios like `V' work fine and we don't amass useless
%% tombstones.
-spec gc(ets:tid(), MinVsn, {Key, Vsn}, Key, Older, CurrentDeleted, State) -> ok when
      MinVsn :: pos_integer(),
      Key :: binary(),
      Vsn :: pos_integer(),
      Older :: [Vsn],
      CurrentDeleted :: [Vsn],
      State :: del | keep.
%% Prune old versions
gc(T, MinVsn, {Key, Vsn}, Key, Older, Dels, State) when Vsn < MinVsn ->
    gc(T, MinVsn, ets:next(T, {Key,Vsn}), Key, [Vsn|Older], Dels, State);
%% Current versions, in the first deleted sequence
gc(T, MinVsn, {Key,Vsn}, Key, Older, Dels, del) ->
    flush_deleted(T, Key, Older), % newer entry, older keys superceded
    case is_deleted(T, Key, Vsn) of
        true -> % accumulate deleted stuff
            gc(T, MinVsn, ets:next(T,{Key,Vsn}), Key, [], [Vsn|Dels], del);
        false -> % if a value is not deleted anymore, flush previous deleted
            flush_deleted(T, Key, Dels),
            gc(T, MinVsn, ets:next(T,{Key,Vsn}), Key, [], [], keep)
    end;
%% Current versions, out of deletion sequences -- keep everything
gc(T, MinVsn, {Key,Vsn}, Key, [], [], keep) ->
    gc(T, MinVsn, ets:next(T,{Key,Vsn}), Key, [], [], keep);
%% Change of keys, flush the old values we accumulated if we have
%% seen no new values
gc(T, MinVsn, {NewKey,Vsn}, Key, [NewestOld|Older], [], del) ->
    gc_older(T, Key, NewestOld, Older),
    gc(T, MinVsn, {NewKey,Vsn}, NewKey, [], [], del);
%% Change of keys, possibly having accumulated a delete sequence
gc(T, MinVsn, {NewKey,Vsn}, Key, [], Dels, _) ->
    flush_deleted(T, Key, Dels),
    gc(T, MinVsn, {NewKey,Vsn}, NewKey, [], [], del);
%% Same as a change of key, but at the end of table. Needs a different
%% set of clauses because of step-wise key-tracking.
gc(T, _MinVsn, '$end_of_table', Key, [NewestOld|Older], [], del) ->
    gc_older(T, Key, NewestOld, Older);
gc(T, _MinVsn, '$end_of_table', Key, [], Dels, _) ->
    flush_deleted(T, Key, Dels).

gc_older(T, Key, Newest, Older) ->
    case is_deleted(T, Key, Newest) of
        true -> flush_deleted(T, Key, [Newest|Older]);
        false -> flush_deleted(T, Key, Older)
    end.

%% @private Flush accumulated values to kill.
flush_deleted(_, _, []) -> ok;
flush_deleted(T, K, [Vsn|Vsns]) ->
    ets:delete(T, {K,Vsn}),
    flush_deleted(T, K, Vsns).

%% @private iteration for fold
fold_iter(T, _MaxVsn, {End, Vsn}, End, Fun, State) ->
    case ets:lookup(T, {End, Vsn}) of
        [{_, {value, Val}}] -> Fun(End, Val, State);
        [{_, deleted}] -> State
    end;
fold_iter(T, MaxVsn, {Key, Vsn}, End, Fun, State) ->
    NewState = case ets:lookup(T, {Key, Vsn}) of
        [{_, {value, Val}}] -> Fun(Key, Val, State);
        [{_, deleted}] -> State
    end,
    case next(T, MaxVsn, Key) of
        {NewKey, NewVsn} ->
            fold_iter(T, MaxVsn, {NewKey, NewVsn}, End, Fun, NewState);
        '$end_of_table' ->
            NewState
    end.

