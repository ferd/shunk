%% @doc re-hash a bunch of binaries with a fixed window size where the size
%% for a given chunk has as many entries can be fit entirely in a packet.
%% If size is 30 and we have 2500 chunks, that gives a level of
%% 84 chunks. This level must be re-chunked in batches of ~30, yielding
%% 3.
%%
%% Each level should note:
%% - its level (1 = level above chunks, 2 = chunks of chunks, etc.)
%% - the md5 of the hashes of its subchunks
%% - the count of entries it represents.
%%
%% Later on, when 1 chunk at level 0 is found to be wrong,
%% we can probably do the same, but in reverse.
-module(shunk_snap).

%% API exports
-export([to_levels/3, partial_to_levels/4, to_mlevels/3]).
%% Test/internal exports
-export([diff_lvls/2]).
-ignore_xref([diff_lvls/2]).

-define(WINDOW, 5).
-define(LIMIT, 30).
-type md5() :: <<_:128>>.
-type lvl_val() :: {Start::binary(), End::binary(), md5(), Count::pos_integer()}.
-type lvl() :: {integer(), [lvl_val()]}.
-type range() :: {Start::binary(), End::binary()}.
-export_type([lvl/0, range/0]).

%test() ->
%    io:format("filling table with 2.5M elements..."),
%    T1 = erlang:monotonic_time(milli_seconds),
%    T = bench:fill_table(2500000),
%    T2 = erlang:monotonic_time(milli_seconds),
%    io:format("~pms~nChunking table...", [T2-T1]),
%    C1 = shunk_tab:chunk(T, 1),
%    T3 = erlang:monotonic_time(milli_seconds),
%    io:format("~pms~nBuilding hashed levels...", [T3-T2]),
%    Lvls1 = to_levels(T, 1, C1),
%    T4 = erlang:monotonic_time(milli_seconds),
%    io:format("~pms~nInserting value~n", [T4-T3]),
%    ets:insert(T, {{<<"mycustom-key">>,1},{value,<<"fakeval">>}}),
%    io:format("Re-chunking..."),
%    T5 = erlang:monotonic_time(milli_seconds),
%    Dirty = shunk_tab:mark_dirty(C1, [<<"mycustom-key">>]),
%    C2 = shunk_tab:partial_chunk(T, 1, Dirty),
%    T6 = erlang:monotonic_time(milli_seconds),
%    io:format("~pms~nRe-building hashed levels...", [T6-T5]),
%    Lvls2 = partial_to_levels(T, 1, C2, lists:last(Lvls1)),
%    T7 = erlang:monotonic_time(milli_seconds),
%    io:format("~pms~nDiffing to find bad range...", [T7-T6]),
%    T8 = erlang:monotonic_time(micro_seconds),
%    R = diff_lvls(Lvls1, Lvls2),
%    T9 = erlang:monotonic_time(micro_seconds),
%    io:format("~pµs~nDiffing sublevels to find keys...", [T9-T8]),
%    T10 = erlang:monotonic_time(milli_seconds),
%    D = lists:flatten(
%        [begin
%             M2 = to_mlevels(T, 1, Range),
%             % revert the insert for M2
%             ets:delete(T, {<<"mycustom-key">>,1}),
%             M1 = to_mlevels(T, 1, Range),
%             %% reinsert
%             ets:insert(T, {{<<"mycustom-key">>,1},{value,<<"fakeval">>}}),
%             [Min || {Min,Min} <- shunk_snap:diff_lvls(M1,M2)]
%         end || Range <- R]
%    ),
%    T11 = erlang:monotonic_time(milli_seconds),
%    io:format("~pms~n", [T11-T10]),
%    ets:delete(T),
%    {R,D}.

%% Check the top-level diffs and give a list of ranges
%% to check for differences at lower levels
%%
%% -- The idea might be to determine the shortest continuous
%% range in which a difference is guaranteed to be found.
%% We can then go down and keep comparing these subranges
%% for more direct differences until N lvl0 chunks are found
%% to need inspection.
%%
%%
%% We can then do Lvl < 0 comparisons to find sets of
%% keys that are different?
%%
%% [{0,10,a},{11,20,c},{21,30,t},{31,40,x}]
%% [{0,11,b},{12,20,d},{21,30,t},{31,40,z}]
%% =
%% [{0,20}, {31,40}]
%% ...
%%
%% [{0,5,a},{6,10,b},{11,20,b},{21,25,c},{26,30,d},{31,36,t},{37,40,r}]
%% [{0,5,a},{6,10,c},{11,20,b},{21,25,c},{26,30,d},{31,35,s},{36,40,r}]
%% =>
%% [{0,5,a},{6,10,b},{11,20,b}, {31,36,t},{37,40,r}]
%% [{0,5,a},{6,10,c},{11,20,b}, {31,35,s},{36,40,r}]
%% =
%% [{6,10}, {31,40}] (last = diff length)
%%
%% ----
%%
%% [{0,10,a},{11,20,c},{21,30,t},{31,40,x}]
%% [{0,23,d},{23,30,f},{31,40,x}]
%% =
%% [{0,30}]

%%% - an initial table is built
%%% - the table is then hashed
%%% - subsequent destructive operations are noted in a buffer of dirty entries
%%% - partial chunking can be done on the rest and update all >= levels
%%%   incrementally (and faster than a full scan)
%%%
%%% This requires figuring out how to partial-chunk first rather than full
%%% table chunk:
%%%  - chunk-from-dirty-until-any-stable
%%%  - pass in a bunch of stable chunks and the dirty entries
%%%  - mark chunks candidate for a rewrite
%%%  - re-chunk the entry until a new chunk lines up with a stable one
%%%  - note the re-chunked entries
%%%  - re-hash modified re-chunks
%%%  - re-hash upwards from there

%% @doc This is a test function -- we don't expect to run an actual diff
%% over the network with all the data available, but with progressively
%% obtained data (at least from the remote side).
%%
%% It takes two sets of levels and finds the level-0 chunks that
%% have differing content.
-spec diff_lvls([lvl(), ...], [lvl(), ...]) -> [range()].
diff_lvls([{0, LvlL}], [{0, LvlR}]) ->
    lvl_cmp(LvlL, LvlR);
diff_lvls([{-1, LvlL}], [{-1, LvlR}]) ->
    %% At the -1 level, only single entries remain, so they
    %% can be checked directly
    diff_low(LvlL, LvlR);
diff_lvls([{N, LvlL}, {N2,LvlL2} | L],
          [{N, LvlR}, {N2,LvlR2} | R]) when N =/= 0 ->
    Range = lvl_cmp(LvlL, LvlR),
    diff_lvls([{N2, filter_ranges(LvlL2, Range)} | L],
              [{N2, filter_ranges(LvlR2, Range)} | R]);
diff_lvls([{NL, LvlL}|L], [{NR, LvlR}|R]) when NL > 0, NR > 0 ->
    if NL > NR -> diff_lvls(L, [{NR, LvlR}|R])
    ;  NL < NR -> diff_lvls([{NL, LvlL}|L], R)
    end;
diff_lvls([{NL, LvlL}|L], [{NR, LvlR}|R]) when NL < 0, NR < 0 ->
    if NL < NR -> diff_lvls(L, [{NR, LvlR}|R])
    ;  NL > NR -> diff_lvls([{NL, LvlL}|L], R)
    end.

diff_low([], Ys) -> Ys;
diff_low(Xs, []) -> Xs;
diff_low([X|Xs], [X|Ys]) -> diff_low(Xs, Ys);
diff_low([X={KX,_,_,_}|Xs], [Y={KY,_,_,_}|Ys]) ->
    if KX =:= KY -> [{KX,KY} | diff_low(Xs, Ys)];
       KX < KY -> [{KX, KX} | diff_low(Xs, [Y|Ys])];
       KX > KY -> [{KY, KY} | diff_low([X|Xs], Ys)]
    end.


%% @doc Takes two levels and finds the differing ranges that should
%% be dug into at a lower level.
-spec lvl_cmp([lvl_val()], [lvl_val()]) -> [range()].
lvl_cmp(L, R) -> lists:reverse(same(L, R, [])).

%% @doc Given a level and ranges as returned by `lvl_cmp/2',
%% returns the filtered out level for entries that match a given
%% range.
-spec filter_ranges([lvl_val()], [range()]) -> [lvl_val()].
filter_ranges(_, []) ->
    %% all ranges covered
    [];
filter_ranges([], [_]) ->
    %% last range matched the end of the list of values
    [];
filter_ranges([{_Start,End,_,_}|Lvls], [{Min,_Max}|_]=Range)
  when End < Min -> % current chunk does not fit in
    filter_ranges(Lvls, Range);
filter_ranges([{Start,_End,_,_}|_]=Lvls, [{_Min,Max}|Ranges])
  when Start > Max -> % range is too small for current entries
    filter_ranges(Lvls, Ranges);
filter_ranges([Chunk|Lvls], Range) -> % fits!
    [Chunk | filter_ranges(Lvls, Range)].

%% @private state machine state iterating through matching elements
%% of two levels. As long as the elements are the same, we keep
%% dropping them. If we find two non-matching element, we switch
%% to the `conflict' state.
-spec same([lvl_val()], [lvl_val()], [range()]) -> [range()].
same([], [], Acc) ->
    Acc;
same([H|LT], [H|RT], Acc) ->
    same(LT, RT, Acc);
same([{LStart, LEnd, _LMD5, _LCnt}|_] = L, [], Acc) ->
    conflict(L, [], LStart, LEnd, Acc);
same([], [{RStart, REnd, _RMD5, _RCnt}|_] = R, Acc) ->
    conflict([], R, RStart, REnd, Acc);
same([{LStart, LEnd, _LMD5, _LCnt}|_] = L,
     [{RStart, REnd, _RMD5, _RCnt}|_] = R, Acc) ->
    %% Store the smallest start value; the conflict state only
    %% looks for the end of it
    conflict(L, R, min(LStart, RStart), max(LEnd, REnd), Acc).

%% @private look for the end of a non-overlapping sequence.
%% This is done eagerly, where we keep holding on to the furthest
%% non-matching endpoint. We also drop the smallest endpoint seen
%% yet since matching ranges have to start becoming in sync again
%% at some point. Everything in between is a potential conflict
%% TODO: try with a less eager version since a uniform distribution
%% may yield ranges that cover the whole table and defeat the purpose
-spec conflict([lvl_val()], [lvl_val()], binary(), binary(), [range()]) -> [range()].
conflict([], [], Min, Max, Acc) ->
    [{Min,Max}|Acc];
conflict([], [{_, REnd, _, _}|TR], Min, Max, Acc) ->
    conflict([], TR, Min, max(Max, REnd), Acc);
conflict([{_, LEnd, _, _}|TL], [], Min, Max, Acc) ->
    conflict(TL, [], Min, max(Max, LEnd), Acc);
conflict([H|LT], [H|RT], Min, Max, Acc) ->
    same(LT, RT, [{Min, Max}|Acc]);
conflict([{_LStart, LEnd, _LMD5, _LCnt}|LT] = L,
         [{_RStart, REnd, _RMD5, _RCnt}|RT] = R,
         Min, Max, Acc) ->
    if LEnd > REnd -> % L-side goes further, shed right
        conflict(L, RT, Min, max(LEnd, Max), Acc)
     ; LEnd < REnd -> % R-side goes further, shed left
        conflict(LT, R, Min, max(REnd, Max), Acc)
     ; LEnd =:= REnd -> % drop both
        %% maybe the next ones match; end the current range
        %% optimistically
        same(LT, RT, [{Min, max(LEnd, REnd)}|Acc])
    end.

%% @doc Takes a `shunk_tab' table, the  current `MaxVsn', and
%% chunks for the table, and returns all levels above 0 for that table.
-spec to_levels(ets:tid(), pos_integer(), [shunk_tab:chunk()]) -> [lvl()].
to_levels(T, MaxVsn, Chunks) ->
    Lvl0 = lvl0(T,MaxVsn,Chunks),
    reduce(?LIMIT, Lvl0, [Lvl0]).

%% @doc Takes a `shunk_tab' table, the  current `MaxVsn', annotated
%% chunks for the table, and a previously calculated set of levels,
%% and returns all levels above 0 for that table.
%%
%% This allows to do only partial re-hashing of the table at level 0,
%% which is the most expensive step of computation.
-spec partial_to_levels(ets:tid(), pos_integer(), [shunk_tab:annotated_chunk()],
                        {0, [lvl_val()]}) -> [lvl()].
partial_to_levels(T, MaxVsn, AnnotatedChunks, OldLvl0) ->
    Lvl0 = partial_lvl0(T, MaxVsn, AnnotatedChunks, OldLvl0),
    reduce(?LIMIT, Lvl0, [Lvl0]).

-spec to_mlevels(ets:tid(), pos_integer(), term()) -> [lvl()].
to_mlevels(T, MaxVsn, Chunks) ->
    Lvlm1 = lvlm1(T,MaxVsn,Chunks),
    reduce_down(?LIMIT, Lvlm1, [Lvlm1]).

%% @private recursively re-chunks levels until we have a top-level
%% one under `?LIMIT' in size.
-spec reduce(pos_integer(), lvl(), [lvl()]) -> [lvl()].
reduce(Limit, {Lvl,LvlData}, Acc) ->
    Size = length(LvlData),
    if Size =< Limit ->
           Acc;
       Size > Limit ->
           NewLvl = lvlup({Lvl,LvlData}),
           reduce(Limit, NewLvl, [NewLvl|Acc])
    end.

%% @private recursively re-chunks levels until we have a top-level
%% one under `?LIMIT' in size.
-spec reduce_down(pos_integer(), lvl(), [lvl()]) -> [lvl()].
reduce_down(Limit, {Lvl,LvlData}, Acc) ->
    Size = length(LvlData),
    if Size =< Limit ->
           Acc;
       Size > Limit ->
           NewLvl = lvldown({Lvl,LvlData}),
           reduce_down(Limit, NewLvl, [NewLvl|Acc])
    end.

%% @doc generates the first level (0) of data by iterating over every
%% chunks and generating MD5s of the full set.
-spec lvl0(ets:tid(), pos_integer(), [shunk_tab:chunk()]) -> lvl().
lvl0(T, MaxVsn, Chunks) ->
    case shunk_tab:first(T, MaxVsn) of
        {_First,_Vsn} ->
            {0,
             [md5(Start, End, N, shunk_tab:fold(T, MaxVsn, Start, End, fun acc/3, []))
              || {Start, End, N} <- Chunks]};
        '$end_of_table' ->
            {0, []}
    end.

%% @doc generates the first level (0) of data using annotated chunks
%% (clean or dirty) and the previous levels from a prior run, only
%% re-hashing chunks that are not the same as prior ones.
%%
%% This saves a lot of work since iterating and hashing the table is
%% more expensive than just looking up unchanged values in a list.
partial_lvl0(T, MaxVsn, AnnotatedChunks, {0, OldLvl0}) ->
    Lvl = partial_run(T, MaxVsn, AnnotatedChunks, OldLvl0),
    {0, Lvl}.

partial_run(_, _, [], _) -> [];
partial_run(T, MaxVsn, [Clean={clean,{Start,_,_}}|Normalized],
                       [Entry={EntryStart,_,_,_}|OldLvl0]) ->
    %% The entry *must* be there
    if Start =:= EntryStart ->
           [Entry | partial_run(T, MaxVsn, Normalized, OldLvl0)];
       Start > EntryStart ->
           partial_run(T, MaxVsn, [Clean|Normalized], OldLvl0)
    end;
partial_run(T, MaxVsn, [{dirty,{Start,End,N}}|Normalized], OldLvl0) ->
    [md5(Start, End, N, shunk_tab:fold(T, MaxVsn, Start, End, fun acc/3, []))
     | partial_run(T, MaxVsn, Normalized, OldLvl0)].

%% @private Takes a level, re-chunks it, and re-MD5s it.
%% The re-chunking is done based on the current boundaries, but ignores
%% the MD5 such that levels above the current one are modified based
%% on shifting chunk boundaries, but not their content.
%%
%% This allows to reduce the number of entries while keeping respect
%% to lower level chunks, but account for internal changes independently,
%% and ultimately leads to the ability to do proper range checks.
-spec lvlup(lvl()) -> lvl().
lvlup({N, Lvl}) when N >= 0 ->
    Chunks = iterate(list_to_tuple(Lvl), shunk_chunk:init(?WINDOW, ?LIMIT), 1, []),
    {N+1, md5_chunks(Chunks, Lvl)}.

%% @private For low-level windows (-1, -2, ...) we may want to go for a TTTD
%% window again, over the contents of a chunk range, calculated
%% on the fly. Has to be done from top-level down to per-row
%% level where the rows fit within `?LIMIT'
%%
%% There will be one lvl-1 stack per valid range, as opposed to one for
%% the full table.
lvlm1(T, MaxVsn, {Start, End}) ->
    %% Fetch all the values in the range and have them chunked according
    %% to `?LIMIT'
    %% MD5 said limit
    %% do the up-level thing, except going -2, -3, -4, ...
    %% -- Assume that the data stored in the table is already md5'd
    KVs = lists:reverse(
        shunk_tab:fold(T, MaxVsn, Start, End, fun acc_4tup/3, [])
    ),
    {-1, KVs}.

lvldown({N, Lvl}) when N < 0 ->
    {_, NewLvl} = lvlup({0, Lvl}),
    {N-1, NewLvl}.

%% @private puts two elements in a list in order ready to
%% be reversed. Just used in a fold.
acc(K, V, Acc) -> [V, K | Acc].
%% @private puts two elements in a list as a pair
acc_4tup(K, V, Acc) -> [{K, K, V, 1} | Acc].
%acc_md5(K, V, Acc) -> [{K, K, crypto:hash(md5, [K,V]), 1} | Acc].
%acc_tup(K, V, Acc) -> [{K, V} | Acc].

%% @private Hashes the full set of elements.
md5(Start, End, N, List) ->
    {Start, End, crypto:hash(md5, lists:reverse(List)), N}.

%% @private Break up a level into chunks with only the last key of each being
%% stored. This list can then be used to run md5s on the list of prior chunks.
%% This is simpler to do in two passes since chunks can be broken at points
%% back in the past.
%%
%% Chunking is disjoint from MD5ing so that if there's a difference in chunk
%% content but not chunk boundaries, chunk boundaries are not modified
%% (though we should be able to deal with this too)
-spec iterate(tuple(), shunk_chunk:tttd(), pos_integer(), [binary()]) -> [binary()].
iterate(Bins, _State, Pos, Chunks) when Pos =:= tuple_size(Bins) ->
    {_Start, Stop, _Md5, _Count} = element(Pos, Bins),
    lists:reverse([Stop | Chunks]);
iterate(Bins, State, Pos, Chunks) ->
    {Start, Stop, _MD5, _Count} = element(Pos, Bins),
    case shunk_chunk:chunk([Start,Stop], State) of
        {next, NewState} ->
            iterate(Bins, NewState, Pos+1, Chunks);
        {chunk, Offset, [_Start,NewStop], NewState} ->
            iterate(Bins, NewState, (Pos+Offset)+1, [NewStop|Chunks])
    end.

%% @private Return the MD5 rolling hash of all the keys + values in a sequence
%% of chunks. Preps `md5_chunks/3'
-spec md5_chunks([binary()], [lvl_val()]) -> [lvl_val()].
md5_chunks(Chunks, Lvl) ->
    lists:reverse(md5_chunks(Chunks, Lvl, [])).

%% @private Chunks are end-delimited; we start a new sequence in this function
%% by using the current level's first entry; preps `md5_chunks/4'
%% iterations. `md5_chunks/4' calls back this one in mutual recursion
%% every time a chunk is done.
md5_chunks([], [], Acc) -> Acc;
md5_chunks([Stop|Chunks], [{Start,_,_,_}|_]=Lvl, Acc) ->
    md5_chunks([Stop|Chunks], Lvl, {Start,[],0}, Acc).

%% @private Take all the MD5 values of each entry in the chunks.
%% When the chunk sequence is over, they get MD5'd at once.
md5_chunks([Stop|Chunks], [{_Start, Stop, MD5, Count}|Lvls], {Start,MD5s,N}, Acc) ->
    New = {Start, Stop, crypto:hash(md5, lists:reverse([MD5|MD5s])), Count+N},
    md5_chunks(Chunks, Lvls, [New|Acc]);
md5_chunks(Chunks, [{_Start, _Stop, MD5, Count}|Lvls], {Start,MD5s,N}, Acc) ->
    md5_chunks(Chunks, Lvls, {Start, [MD5|MD5s],N+Count}, Acc).
