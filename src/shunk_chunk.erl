%%% @doc Implementation of content-sensitive chunking based on the TTTD
%%% algorithm outlined in
%%% [http://www.hpl.hp.com/techreports/2005/HPL-2005-30R1.html]. The values
%%% chosen for the algorithm (threshold and divisors) are those outlined
%%% in the Annex. Instead of using Rabin's fingerprint (which is slower to
%%% implement in Erlang) as the rolling hash algorithm, we use adler32,
%%% which is included in the standard library.
%%%
%%% The calling module must be able to provide accessibility to data either
%%% with predictable key-based access, or with an iterator that can move
%%% around by known numbers of hops. The caller can then feed keys and
%%% values one by one as `iodata()' and get the value or relative index
%%% of the value that delimits a chunk's upper bound (inclusive).
-module(shunk_chunk).
-export([init/0, init/2, init/5, chunk/2]).
-export_type([tttd/0]).

-ifndef(r).
-define(r, 0). % hash to compare value to
-endif.

-record(tttd, {window_size = 15 :: pos_integer(),
               buffer = {} :: tuple(),
               nth = 1 :: pos_integer(),
               threshold_low=460 :: pos_integer(),
               threshold_high=2800 :: pos_integer(),
               divisor=540 :: non_neg_integer(),
               backup_divisor=270 :: non_neg_integer(),
               backup_offset=undefined :: undefined | term()}).

-opaque tttd() :: #tttd{}.

%% @doc Create a default state seed for the chunking algorithm.
%% Equivalent to `init(15, 460, 2800, 540, 270)'.
-spec init() -> tttd().
init() -> init(15, 460, 2800, 540, 270).

init(WindowSize, ChunkSize) ->
    init(WindowSize,
         scale(ChunkSize, 460),
         scale(ChunkSize, 2800),
         scale(ChunkSize, 540),
         scale(ChunkSize, 270)).

scale(M, X) ->
    trunc(X * (M/1015)).

-spec init(WindowSize, Low, High, D1, D2) -> tttd() when
      WindowSize :: pos_integer(),
      Low :: pos_integer(),
      High :: pos_integer(),
      D1 :: non_neg_integer(),
      D2 :: non_neg_integer().
init(WindowSize, Low, High, D1, D2) ->
    #tttd{window_size=WindowSize,
          threshold_low=Low, threshold_high=High,
          divisor=D1, backup_divisor=D2}.

%% @doc Returns an iteration of a chunk. The calling function must be able
%% to provide keys as `iodata()'.
%%
%% The value returned is either `{next, NewState}' when the chunk is not yet
%% terminated, or `{chunk, Offset, iodata(), NewState}'.
%%
%% When returning a chunk, either the current value is picked, or if we hit
%% the upper threshold of the sliding window, the backup divisior picks a
%% backup offset. This backup offset is one of the elements already seen
%% before.
%%
%% The `iodata()' returned matches the specific input used when the chunk was
%% detected. If the caller submits `[Key,Value]' and uses key-based access,
%% then `[Key,Value]' will be returned. This is useful when using say, an
%% ETS table, which allows calling `ets:next(Tid, Key)' to get the follow-up
%% element.
%%
%% If the calling code instead relies on specific indices to do iteration,
%% the `Offset' value will be useful. The offset is relative, such that
%% when the current value is picked, the value returned is `0'. If an older
%% one is chosen (due to falling back to a backup divisor), the `Offset'
%% value will be negative. So for example, in a sliding window with an
%% upper threshold of 54 elements, if the backup divisor was at the 44th
%% position, `Offset' will have a value of `-10'.
%%
%% Note: if the iteration finishes before a final chunk can be set, it will
%% be up to the caller to carry the last boundary and set it if they want
%% all of their content to be included.
%%
%% Note: The window size is incremented only once per entry rather than
%% once per byte, since it makes more sense semantically for a key-value
%% store.
-spec chunk(Data, State) -> {chunk, Offset, Data, State} | {next, State} when
      Data :: iodata(),
      State :: tttd(),
      Offset :: 0 | neg_integer().
chunk(IoData, T=#tttd{window_size=WindowSize, buffer=Buffer, nth=Nth,
                      threshold_low=Low, threshold_high=High,
                      divisor=D1, backup_divisor=D2, backup_offset=Backup}) ->
    Buffer2 = case tuple_size(Buffer) of
        WindowSize ->
            erlang:append_element(erlang:delete_element(1, Buffer), IoData);
        _ ->
            erlang:append_element(Buffer, IoData)
    end,
    if Nth < Low ->
           %% keep going
           {next, T#tttd{buffer=Buffer2, nth=Nth+1}};
       Nth < High ->
           %% grab hashes
           Hash = erlang:adler32(tuple_to_list(Buffer2)),
           case Hash rem D1 =:= ?r of
               true ->
                   %% Chunk end, reset buffer, backup offset, counter
                   {chunk, 0, IoData,
                    T#tttd{buffer={}, nth=1, backup_offset=undefined}};
               false ->
                   NewBackup = case Backup of
                       undefined when Hash rem D2 =:= ?r ->
                           {IoData,Nth,Hash,Buffer2};
                       _ ->
                           Backup
                   end,
                   %% Keep going
                   {next, T#tttd{buffer=Buffer2, nth=Nth+1,
                                 backup_offset=NewBackup}}
           end;
       Nth >= High, Backup =:= undefined ->
           %% Current value is a forced limit
           {chunk, 0, IoData,
            T#tttd{buffer={}, nth=1, backup_offset=undefined}};
       Nth >= High ->
           %% Roll back to the old backup offset
           {OldIo,OldNth,_OldHash,_OldBuffer} = Backup,
           {chunk, OldNth-Nth, OldIo,
            T#tttd{buffer={}, nth=1, backup_offset=undefined}}
    end.

